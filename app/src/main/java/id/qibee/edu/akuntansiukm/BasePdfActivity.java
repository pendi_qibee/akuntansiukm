package id.qibee.edu.akuntansiukm;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import id.qibee.edu.akuntansiukm.db.Expense;
import id.qibee.edu.akuntansiukm.db.Month;
import id.qibee.edu.akuntansiukm.db.Purchase;
import id.qibee.edu.akuntansiukm.db.Sales;
import id.qibee.edu.akuntansiukm.db.Transaction;
import id.qibee.edu.akuntansiukm.viewmodel.ExpenseViewModel;
import id.qibee.edu.akuntansiukm.viewmodel.PurchasesViewModel;
import id.qibee.edu.akuntansiukm.viewmodel.SalesViewModel;
import id.qibee.edu.akuntansiukm.viewmodel.TransactionViewModel;

public class BasePdfActivity extends AppCompatActivity {
    public File pdfFile;
    final public int REQUEST_CODE_ASK_PERMISSIONS = 111;
    public List<Transaction> transactionArrayList = new ArrayList<>();
    public List<Month> monthList = new ArrayList<>();
    public List<Sales> salesList = new ArrayList<>();
    public List<Purchase> purchaseList = new ArrayList<>();
    public List<Expense> expenseList = new ArrayList<>();

    public TransactionViewModel transactionViewModel;
    public PurchasesViewModel purchasesViewModel;
    public ExpenseViewModel expenseViewModel;
    public SalesViewModel salesViewModel;
    public String name="", month="", monthWord="";
    public static String MONTH_EXTRA = "month_date";
    public static String MONTHWORD_EXTRA = "monthword_date";
    public static String SALES_EXTRA = "sales_sum";
    public static String PURCHASE_EXTRA = "purchase_sum";
    public static String EXPENSE_EXTRA = "expense_sum";
    public static String REVENUE_EXTRA = "revenue_sum";
    private static String FILE = "c:/temp/FirstPdf.pdf";
    public static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,
            Font.BOLD);
    public static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.NORMAL, BaseColor.RED);
    public static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16,
            Font.BOLD);
    public static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.BOLD);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
}
