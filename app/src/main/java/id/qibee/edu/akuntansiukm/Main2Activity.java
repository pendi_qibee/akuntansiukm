package id.qibee.edu.akuntansiukm;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.qibee.edu.akuntansiukm.view.ExpenseAllFragment;
import id.qibee.edu.akuntansiukm.view.ProductAllFragment;
import id.qibee.edu.akuntansiukm.view.PurchaseAllFragment;
import id.qibee.edu.akuntansiukm.view.SalesAllFragment;

public class Main2Activity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.main_container)
    FrameLayout mainContainer;
    FragmentManager manager = getSupportFragmentManager();
    String title="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        ButterKnife.bind(this);
        initActionBar(title);

        String kode = getIntent().getStringExtra("kode");
        setFragment(kode);
    }

    private void initActionBar(String title) {
        ActionBar actionBar = this.getSupportActionBar();
        if (actionBar != null) {
//            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(title);
        }
    }

    private void setFragment(String kode) {
        switch (kode) {
            case "penjualan":
                initActionBar("Data Penjualan");
                manager.beginTransaction()
                        .replace(R.id.main_container, new SalesAllFragment())
                        .commit();
                break;

            case "pembelian":
                initActionBar("Data Pembelian");
                manager.beginTransaction()
                        .replace(R.id.main_container, new PurchaseAllFragment())
                        .commit();
                break;
            case "pengeluaran":
                initActionBar("Data Pengeluaran");
                manager.beginTransaction()
                        .replace(R.id.main_container, new ExpenseAllFragment())
                        .commit();
                break;
            case "produk":
                initActionBar("Data Produk");
                manager.beginTransaction()
                        .replace(R.id.main_container, new ProductAllFragment())
                        .commit();
                break;

                case "guide":
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://drive.google.com/open?id=18phOd7peV6T52DnsaL8au_vwzlc5t3nz"));
                            startActivity(intent);
                break;
        }
    }
}
