package id.qibee.edu.akuntansiukm;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.qibee.edu.akuntansiukm.view.ExpenseAllFragment;
import id.qibee.edu.akuntansiukm.view.MonthlyReportActivity;
import id.qibee.edu.akuntansiukm.view.MonthlyRevenueActivity;
import id.qibee.edu.akuntansiukm.view.ProductAllFragment;
import id.qibee.edu.akuntansiukm.view.PurchaseAllFragment;
import id.qibee.edu.akuntansiukm.view.RevenueAllFragment;
import id.qibee.edu.akuntansiukm.view.SalesAllFragment;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener, RevenueAllFragment.OnItemSelectedListener {
    //    private ActivityMainBinding layoutBinding;
    FragmentManager manager = getSupportFragmentManager();
    @BindView(R.id.main_container)
    FrameLayout mainContainer;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigation;
    @BindView(R.id.bottom_layout)
    LinearLayout bottomLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        layoutBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

//        initToolbar();
        bottomNavigation.setOnNavigationItemSelectedListener(this);
        bottomNavigation.setSelectedItemId(R.id.rekap_bulanan);

    }

//    private void initToolbar() {
//        final Toolbar toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        final ActionBar actionBar = getSupportActionBar();
//
//        if (actionBar != null) {
//            actionBar.setHomeAsUpIndicator(R.drawable.ic_home);
//            actionBar.setDisplayHomeAsUpEnabled(true);
//        }
//    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.pembelian) {
            manager.beginTransaction()
                    .replace(R.id.main_container, new PurchaseAllFragment())
                    .commit();
        } else if (item.getItemId() == R.id.penjualan) {
            manager.beginTransaction()
                    .replace(R.id.main_container, new SalesAllFragment())
                    .commit();
        } else if (item.getItemId() == R.id.pengeluaran) {
            manager.beginTransaction()
                    .replace(R.id.main_container, new ExpenseAllFragment())
                    .commit();
        } else if (item.getItemId() == R.id.rekap_bulanan) {
            manager.beginTransaction()
                    .replace(R.id.main_container, new RevenueAllFragment())
                    .commit();
//        } else if (item.getItemId() == R.id.pendapatan) {
//            manager.beginTransaction()
//                    .replace(R.id.main_container, new RevenueAllFragment())
//                    .commit();

        } else if (item.getItemId() == R.id.produk) {
            manager.beginTransaction()
                    .replace(R.id.main_container, new ProductAllFragment())
                    .commit();
        }
        return true;
    }

    @Override
    public void onCardClick(String tag) {
        Intent intent = new Intent(this, Main2Activity.class);
        switch (tag) {
            case "rekap_bulanan":
                startActivity(new Intent(this, MonthlyReportActivity.class));
//                manager.beginTransaction()
//                        .replace(R.id.main_container, new MounthlyReportFragment())
//                        .commit();
                break;
            case "transaksi":
                startActivity(new Intent(this, TransactionActivity.class));
                break;

            case "labarugi":
                startActivity(new Intent(this, MonthlyRevenueActivity.class));
                break;
            case "penjualan":
                intent.putExtra("kode", "penjualan");
                startActivity(intent);
                break;
            case "pembelian":
                intent.putExtra("kode", "pembelian");
                startActivity(intent);
                break;
            case "pengeluaran":
                intent.putExtra("kode", "pengeluaran");
                startActivity(intent);
                break;
            case "produk":
                intent.putExtra("kode", "produk");
                startActivity(intent);
                break;
        }
    }
}
