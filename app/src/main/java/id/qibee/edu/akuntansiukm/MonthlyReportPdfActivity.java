package id.qibee.edu.akuntansiukm;

import android.Manifest;
import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import id.qibee.edu.akuntansiukm.db.Expense;
import id.qibee.edu.akuntansiukm.db.Purchase;
import id.qibee.edu.akuntansiukm.db.Sales;
import id.qibee.edu.akuntansiukm.util.Helper;
import id.qibee.edu.akuntansiukm.viewmodel.ExpenseViewModel;
import id.qibee.edu.akuntansiukm.viewmodel.PurchasesViewModel;
import id.qibee.edu.akuntansiukm.viewmodel.SalesViewModel;
import timber.log.Timber;

public class MonthlyReportPdfActivity extends BasePdfActivity {

    private SalesViewModel salesViewModel;
    private List<Sales> salesList = new ArrayList<>();
    private List<Purchase> purchaseList = new ArrayList<>();
    private List<Expense> expenseList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        month = getIntent().getStringExtra(MONTH_EXTRA);
        monthWord = getIntent().getStringExtra(MONTH_EXTRA);
        salesViewModel = ViewModelProviders.of(this).get(SalesViewModel.class);
        purchasesViewModel = ViewModelProviders.of(this).get(PurchasesViewModel.class);
        expenseViewModel = ViewModelProviders.of(this).get(ExpenseViewModel.class);

        Bundle bundle = new Bundle();
        bundle.putString(MONTH_EXTRA, month);
        bundle.putString(MONTHWORD_EXTRA, monthWord);

        salesViewModel.getSales(month).observe(this, sales -> {
            salesList = sales;
            preparePdf(salesList);

        });


    }

    private void preparePdf(List<Sales> salesList) {
        purchasesViewModel.getPurchasesInMonth(month).observe(this, purchases -> {
            purchaseList = purchases;
            preparePdf2(salesList, purchaseList);
        });
    }

    @SuppressLint("StaticFieldLeak")
    private void preparePdf2(List<Sales> salesList, List<Purchase> purchaseList) {
        expenseViewModel.getExpenseInMonth(month).observe(this, expenses -> {
            expenseList = expenses;

//            new AsyncTask<Void, Void, Void>(){
//                @Override
//                protected Void doInBackground(Void... voids) {
                    try {
                        createPdfWrapper(salesList, purchaseList, expenseList);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (DocumentException e) {
                        e.printStackTrace();
                    }
//
//                    return null;
//                }
//            }.execute();


        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    Toast.makeText(this, "WRITE_EXTERNAL Permission Allowed", Toast.LENGTH_SHORT)
                            .show();
                    recreate();
                } else {
                    // Permission Denied
                    Toast.makeText(this, "WRITE_EXTERNAL Permission Denied", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
    private void createPdfWrapper(List<Sales> salesList, List<Purchase> purchaseList, List<Expense> expenseList) throws FileNotFoundException, DocumentException {
        int hasWriteStoragePermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (hasWriteStoragePermission != PackageManager.PERMISSION_GRANTED) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!shouldShowRequestPermissionRationale(Manifest.permission.WRITE_CONTACTS)) {
                    showMessageOKCancel("You need to allow access to Storage",
                            (dialog, which) -> {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,},
                                            REQUEST_CODE_ASK_PERMISSIONS);
                                }
                            });
                    return;
                }


                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_CODE_ASK_PERMISSIONS);
            }
        } else {
            createPdf(salesList, purchaseList, expenseList);
            finish();
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void createPdf(List<Sales> salesList, List<Purchase> purchaseList, List<Expense> expenseList) throws FileNotFoundException, DocumentException {

        File docsFolder = new File(Environment.getExternalStorageDirectory() + "/Documents");
        if (!docsFolder.exists()) {
            docsFolder.mkdir();
            Timber.i("Created a new directory for PDF");
        }

        pdfFile = new File(docsFolder.getAbsolutePath(), "Laporan_bulanan" + System.currentTimeMillis() + ".pdf");
        OutputStream output = new FileOutputStream(pdfFile);
        Document document = new Document();
        PdfWriter.getInstance(document, output);
        document.open();
        addContent(document, salesList, purchaseList, expenseList);
        document.close();
//        previewPdf();

    }

    private void addContent(Document document, List<Sales> salesList, List<Purchase> purchaseList, List<Expense> expenseList) throws DocumentException {
        Anchor anchor = new Anchor("Laporan Bulanan " + name, catFont);
        anchor.setName("laporan transaksi");
        // Second parameter is the number of the chapter
        Chapter catPart = new Chapter(new Paragraph(anchor), 3);

        Paragraph subPara = new Paragraph("Laporan Bulan " + monthWord, subFont);
        Section subCatPart = catPart.addSection(subPara);

        Paragraph preface = new Paragraph("LAPORAN PENJUALAN");
        preface.setAlignment(Element.ALIGN_CENTER);
        addEmptyLine(preface, 1);
        subCatPart.add(preface);

        createSalesTable(subCatPart, salesList);

        Paragraph preface2 = new Paragraph("LAPORAN PEMBELIAN");
        preface2.setAlignment(Element.ALIGN_CENTER);
        addEmptyLine(preface2, 1);
        subCatPart.add(preface2);

        createPurchaseTable(subCatPart, purchaseList);

        Paragraph preface3 = new Paragraph("LAPORAN BIAYA-BIAYA");
        preface3.setAlignment(Element.ALIGN_CENTER);
        addEmptyLine(preface3, 1);
        subCatPart.add(preface3);
        createExpenseTable(subCatPart, expenseList);

        // now add all this to the document
        document.add(catPart);
    }

    private void createSalesTable(Section subCatPart, List<Sales> salesList)
            throws BadElementException {
        PdfPTable table = new PdfPTable(3);

        PdfPCell c1 = new PdfPCell(new Phrase("TANGGAL"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setPadding(10);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("KETERANGAN"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setPadding(10);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("JUMLAH"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setPadding(10);
        table.addCell(c1);

        table.setHeaderRows(1);

        Timber.d("salesList size %s", salesList.size());
        for (Sales sales1 : salesList) {

            PdfPCell c2 = new PdfPCell(new Phrase(Helper.getStringFromDate(sales1.getDateTime())));
            c2.setHorizontalAlignment(Element.ALIGN_CENTER);
            c2.setPadding(10);
            table.addCell(c2);

            c2 = new PdfPCell(new Phrase(sales1.getProductSold()));
            c2.setHorizontalAlignment(Element.ALIGN_LEFT);
            c2.setPadding(10);
            table.addCell(c2);

            c2 = new PdfPCell(new Phrase(Helper.formatRupiah(sales1.getTotalCash())));
            c2.setHorizontalAlignment(Element.ALIGN_RIGHT);
            c2.setPadding(10);
            table.addCell(c2);
        }


        subCatPart.add(table);
    }


    private static void createPurchaseTable(Section subCatPart, List<Purchase> purchaseList)
            throws BadElementException {
        PdfPTable table = new PdfPTable(3);

        PdfPCell c1 = new PdfPCell(new Phrase("TANGGAL"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setPadding(10);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("KETERANGAN"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setPadding(10);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("JUMLAH"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setPadding(10);
        table.addCell(c1);

        table.setHeaderRows(1);

//        Timber.d("sales sold %s", purchaseList.get(0).getProductSold());
        Timber.d("purchaseList size %s", purchaseList.size());
        for (Purchase purchase : purchaseList) {

            PdfPCell c2 = new PdfPCell(new Phrase(Helper.getStringFromDate(purchase.getPurchaseDate())));
            c2.setHorizontalAlignment(Element.ALIGN_CENTER);
            c2.setPadding(10);
            table.addCell(c2);

            c2 = new PdfPCell(new Phrase(purchase.getAllProductName()));
            c2.setHorizontalAlignment(Element.ALIGN_LEFT);
            c2.setPadding(10);
            table.addCell(c2);

            c2 = new PdfPCell(new Phrase(Helper.formatRupiah(purchase.getTotalCash())));
            c2.setHorizontalAlignment(Element.ALIGN_RIGHT);
            c2.setPadding(10);
            table.addCell(c2);
        }
        subCatPart.add(table);
    }

    private static void createExpenseTable(Section subCatPart, List<Expense> expenseList)
            throws BadElementException {
        PdfPTable table = new PdfPTable(3);

        PdfPCell c1 = new PdfPCell(new Phrase("TANGGAL"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setPadding(10);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("KETERANGAN"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setPadding(10);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("JUMLAH"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setPadding(10);
        table.addCell(c1);

        table.setHeaderRows(1);

//        Timber.d("sales sold %s", purchaseList.get(0).getProductSold());
        Timber.d("expenseList size %s", expenseList.size());
        for (Expense expense : expenseList) {

            PdfPCell c2 = new PdfPCell(new Phrase(Helper.getStringFromDate(expense.getExpenseDate())));
            c2.setHorizontalAlignment(Element.ALIGN_CENTER);
            c2.setPadding(10);
            table.addCell(c2);

            c2 = new PdfPCell(new Phrase(expense.getCostName()));
            c2.setHorizontalAlignment(Element.ALIGN_LEFT);
            c2.setPadding(10);
            table.addCell(c2);

            c2 = new PdfPCell(new Phrase(Helper.formatRupiah(expense.getCost())));
            c2.setHorizontalAlignment(Element.ALIGN_RIGHT);
            c2.setPadding(10);
            table.addCell(c2);
        }
        subCatPart.add(table);
    }
}
