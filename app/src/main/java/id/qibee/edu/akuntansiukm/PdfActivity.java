package id.qibee.edu.akuntansiukm;

import android.Manifest;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import id.qibee.edu.akuntansiukm.db.Transaction;
import id.qibee.edu.akuntansiukm.util.Helper;
import id.qibee.edu.akuntansiukm.viewmodel.TransactionViewModel;
import timber.log.Timber;

public class PdfActivity extends BasePdfActivity {
    private File pdfFile;
    final private int REQUEST_CODE_ASK_PERMISSIONS = 111;
    public List<Transaction> transactionArrayList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf);
        name = QibeeApp.getInstance().getUsername();

        transactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel.class);
        transactionViewModel.getAllTransactions().observe(this, transactions -> {
            transactionArrayList = transactions;
            try {
                createPdfWrapper(transactionArrayList);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (DocumentException e) {
                e.printStackTrace();
            }

        });


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    try {
                        createPdfWrapper(transactionArrayList);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (DocumentException e) {
                        e.printStackTrace();
                    }
                } else {
                    // Permission Denied
                    Toast.makeText(this, "WRITE_EXTERNAL Permission Denied", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void createPdfWrapper(List<Transaction> transactions) throws FileNotFoundException, DocumentException {
        int hasWriteStoragePermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (hasWriteStoragePermission != PackageManager.PERMISSION_GRANTED) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!shouldShowRequestPermissionRationale(Manifest.permission.WRITE_CONTACTS)) {
                    showMessageOKCancel("You need to allow access to Storage",
                            (dialog, which) -> {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,},
                                            REQUEST_CODE_ASK_PERMISSIONS);
                                }
                            });
                    return;
                }


                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_CODE_ASK_PERMISSIONS);
            }
        } else {
            createPdf(transactions);
            finish();
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void createPdf(List<Transaction> transactions) throws FileNotFoundException, DocumentException {
        Timber.d("transcek2 %s", transactions);

        File docsFolder = new File(Environment.getExternalStorageDirectory() + "/Documents");
        if (!docsFolder.exists()) {
            docsFolder.mkdir();
            Timber.i("Created a new directory for PDF");
        }

        pdfFile = new File(docsFolder.getAbsolutePath(), "Laporan_semua_transaksi"+System.currentTimeMillis()+".pdf");
        OutputStream output = new FileOutputStream(pdfFile);
        Document document = new Document();
        PdfWriter.getInstance(document, output);
        document.open();
//        document.add(new Paragraph(mContentEditText.getText().toString()));
        addMetaData(document);
        addTitlePage(document);
        addContent(document, transactions);
        document.close();
//        previewPdf();

    }



    private static void addMetaData(Document document) {
        document.addTitle("Laporan Akuntansi");
        document.addSubject("Using iText");
        document.addKeywords("Java, PDF, iText");
        document.addAuthor("QIBEE");
        document.addCreator("Pendi Madyana");
    }

    private static void addTitlePage(Document document)
            throws DocumentException {
        Paragraph preface = new Paragraph();
        // We add one empty line
//        addEmptyLine(preface, 1);
        // Lets write a big header
        preface.add(new Paragraph("LAPORAN TRANSAKSI"));
//
//        addEmptyLine(preface, 1);
//        // Will create: Report generated by: _name, _date
//        preface.add(new Paragraph(
//                "Report generated by: " + System.getProperty("user.name") + ", " + new Date(), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
//                smallBold));
//        addEmptyLine(preface, 3);
//        preface.add(new Paragraph(
//                "This document describes something which is very important ",
//                smallBold));
//
//        addEmptyLine(preface, 8);
//
//        preface.add(new Paragraph(
//                "This document is a preliminary version and not subject to your license agreement or any other agreement with vogella.com ;-).",
//                redFont));

//        document.add(preface);
        // Start a new page
//        document.newPage();
    }

    private void addContent(Document document, List<Transaction> transactions) throws DocumentException {
        Anchor anchor = new Anchor("Laporan Transaksi "+name, catFont);
        anchor.setName("laporan transaksi");
        // Second parameter is the number of the chapter
        Chapter catPart = new Chapter(new Paragraph(anchor), 1);

        Paragraph subPara = new Paragraph("Laporan Semua Transaksi", subFont);
        Section subCatPart = catPart.addSection(subPara);

        Paragraph preface = new Paragraph();
        addEmptyLine(preface, 3);
        subCatPart.add(preface);

        // add a table
        createTable(subCatPart, transactions);

        // now add all this to the document
        document.add(catPart);

    }

    private static void createTable(Section subCatPart, List<Transaction> transactions)
            throws BadElementException {
        PdfPTable table = new PdfPTable(4);

        PdfPCell c1 = new PdfPCell(new Phrase("Tanggal"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setPadding(10);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Jenis"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setPadding(10);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Debit"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setPadding(10);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Kredit"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setPadding(10);
        table.addCell(c1);

        table.setHeaderRows(1);
        Timber.d("transcek %s", transactions);
        for (Transaction transaction : transactions) {
            Timber.d("tanggal tr %s", transaction.getDate());
            c1 = new PdfPCell(new Phrase(Helper.getStringFromDate(transaction.getDate())));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setPadding(10);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase(transaction.getType()));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setPadding(10);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase(Helper.formatRupiah(transaction.getCashIn())));
            c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            c1.setPadding(10);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase(Helper.formatRupiah(transaction.getCashOut())));
            c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            c1.setPadding(10);
            table.addCell(c1);

//            table.addCell(transaction.getType());
//            table.addCell(Helper.formatRupiah(transaction.getCashIn()));
//            table.addCell(Helper.formatRupiah(transaction.getCashOut()));

        }

        subCatPart.add(table);

    }




    private void previewPdf() {

        PackageManager packageManager = getPackageManager();
        Intent testIntent = new Intent(Intent.ACTION_VIEW);
        testIntent.setType("application/pdf");
        List list = packageManager.queryIntentActivities(testIntent, PackageManager.MATCH_DEFAULT_ONLY);
        if (list.size() > 0) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            Uri uri = Uri.fromFile(pdfFile);
            intent.setDataAndType(uri, "application/pdf");

            startActivity(intent);
        } else {
            Toast.makeText(this, "Download a PDF Viewer to see the generated PDF", Toast.LENGTH_SHORT).show();
        }
    }


}
