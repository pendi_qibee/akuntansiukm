package id.qibee.edu.akuntansiukm;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import id.qibee.edu.akuntansiukm.logging.NotLoggingTree;
import id.qibee.edu.akuntansiukm.logging.TimberLogImplementation;
import id.qibee.edu.akuntansiukm.util.Constants;
import timber.log.Timber;

public class QibeeApp extends Application implements Constants {
    private static QibeeApp mInstance;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    private String username, userField;
public static String TAG="qib";
    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        sharedPref = this.getSharedPreferences(AKUNTUKM_PREF, Context.MODE_PRIVATE);




        if (BuildConfig.DEBUG) {
//            Timber.plant(new Timber.DebugTree() {
//                @Override
//                protected String createStackElementTag(StackTraceElement element) {
//                    return String.format("C:%s:%s",
//                            super.createStackElementTag(element),
//                            element.getLineNumber());
//                }
//            });
            Timber.plant(new Timber.DebugTree());
            TimberLogImplementation.init();
        } else {
            Timber.plant(new NotLoggingTree());
        }

    }

    public static synchronized QibeeApp getInstance() {
        return mInstance;
    }

    public String getUsername() {
        if (sharedPref.contains(USERNAME)) {
            username = sharedPref.getString(USERNAME, "NAMA USAHA");
        }
        return username;
    }

    public String getUserField() {
        if (sharedPref.contains(USERFIELD)) {
            userField = sharedPref.getString(USERFIELD, "Bidang Usaha / Spesialisasi");
        }
        return userField;
    }


    public void setProfil(String userName, String userField){
        editor=sharedPref.edit();
        editor.putString(USERNAME, userName);
        editor.putString(USERFIELD, userField);
        editor.apply();
    }
}
