package id.qibee.edu.akuntansiukm;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import id.qibee.edu.akuntansiukm.util.Helper;
import timber.log.Timber;

public class RevenueMounthlyPdfActivity extends BasePdfActivity {
    int salesMonth, purchaseMonth, expenseMonth, revenueMonth;
public String month;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_revenue_mounthly_pdf);
        name = QibeeApp.getInstance().getUsername();

        month = getIntent().getStringExtra(MONTH_EXTRA);
        salesMonth = getIntent().getIntExtra(SALES_EXTRA, 0);
        purchaseMonth = getIntent().getIntExtra(PURCHASE_EXTRA, 0);
        expenseMonth = getIntent().getIntExtra(EXPENSE_EXTRA, 0);
        revenueMonth = getIntent().getIntExtra(REVENUE_EXTRA, 0);

        Bundle bundle = new Bundle();
        bundle.putString(MONTH_EXTRA,month);
        bundle.putInt(SALES_EXTRA,salesMonth);
        bundle.putInt(PURCHASE_EXTRA,purchaseMonth);
        bundle.putInt(EXPENSE_EXTRA,expenseMonth);
        bundle.putInt(REVENUE_EXTRA,revenueMonth);

        try {
            createPdfWrapper(bundle);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private void createPdfWrapper(Bundle bundle) throws FileNotFoundException, DocumentException {
        int hasWriteStoragePermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (hasWriteStoragePermission != PackageManager.PERMISSION_GRANTED) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!shouldShowRequestPermissionRationale(Manifest.permission.WRITE_CONTACTS)) {
                    showMessageOKCancel("You need to allow access to Storage",
                            (dialog, which) -> {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,},
                                            REQUEST_CODE_ASK_PERMISSIONS);
                                }
                            });
                    return;
                }


                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_CODE_ASK_PERMISSIONS);
            }
        } else {
            createPdf(bundle);
            finish();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    // Permission Granted
                    Toast.makeText(this, "WRITE_EXTERNAL Permission Allowed", Toast.LENGTH_SHORT)
                            .show();
                    recreate();
                } else {
                    // Permission Denied
                    Toast.makeText(this, "WRITE_EXTERNAL Permission Denied", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void createPdf(Bundle bundle) throws FileNotFoundException, DocumentException {

        File docsFolder = new File(Environment.getExternalStorageDirectory() + "/Documents");
        if (!docsFolder.exists()) {
            docsFolder.mkdir();
            Timber.i("Created a new directory for PDF");
        }

        pdfFile = new File(docsFolder.getAbsolutePath(), "Laporan_laba_rugi"+System.currentTimeMillis()+".pdf");
        OutputStream output = new FileOutputStream(pdfFile);
        Document document = new Document();
        PdfWriter.getInstance(document, output);
        document.open();
        addContent(document, bundle);
        document.close();
//        previewPdf();

    }

    private void addContent(Document document, Bundle bundle) throws DocumentException{
        Anchor anchor = new Anchor("Laporan Laba/Rugi "+name, catFont);
        anchor.setName("laporan transaksi");
        // Second parameter is the number of the chapter
        Chapter catPart = new Chapter(new Paragraph(anchor), 2);

        Paragraph subPara = new Paragraph("Laporan Bulan "+month, subFont);
        Section subCatPart = catPart.addSection(subPara);

        Paragraph preface = new Paragraph();
        addEmptyLine(preface, 3);
        subCatPart.add(preface);

        // add a table
        createTable(subCatPart, bundle);

        // now add all this to the document
        document.add(catPart);
    }

    private static void createTable(Section subCatPart, Bundle bundle)
            throws BadElementException {
        PdfPTable table = new PdfPTable(2);

        PdfPCell c1 = new PdfPCell(new Phrase("JENIS TRANSAKSI"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setPadding(10);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("JUMLAH"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setPadding(10);
        table.addCell(c1);

        table.setHeaderRows(1);

            c1 = new PdfPCell(new Phrase("Penjualan"));
            c1.setHorizontalAlignment(Element.ALIGN_LEFT);
            c1.setPadding(10);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase(Helper.formatRupiah(bundle.getInt(SALES_EXTRA, 0))));
            c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            c1.setPadding(10);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("Pembelian"));
            c1.setHorizontalAlignment(Element.ALIGN_LEFT);
            c1.setPadding(10);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase(Helper.formatRupiah(bundle.getInt(PURCHASE_EXTRA,0))));
            c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            c1.setPadding(10);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("Biaya-biaya"));
            c1.setHorizontalAlignment(Element.ALIGN_LEFT);
            c1.setPadding(10);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase(Helper.formatRupiah(bundle.getInt(EXPENSE_EXTRA,0))));
            c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            c1.setPadding(10);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("Laba / Rugi", smallBold));
            c1.setHorizontalAlignment(Element.ALIGN_LEFT);
            c1.setPadding(10);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase(Helper.formatRupiah(bundle.getInt(REVENUE_EXTRA,0)), smallBold));
            c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            c1.setPadding(10);
            table.addCell(c1);

        subCatPart.add(table);
        }


    }


