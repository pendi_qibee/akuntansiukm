package id.qibee.edu.akuntansiukm;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.qibee.edu.akuntansiukm.db.Transaction;
import id.qibee.edu.akuntansiukm.util.Helper;
import id.qibee.edu.akuntansiukm.view.TransactionReportRvAdapter;
import id.qibee.edu.akuntansiukm.viewmodel.TransactionViewModel;

public class TransactionActivity extends AppCompatActivity {

    @BindView(R.id.rv_transaction)
    RecyclerView rvTransaction;
    @BindView(R.id.text_total_in)
    TextView textTotalIn;
    @BindView(R.id.text_total_out)
    TextView textTotalOut;
    @BindView(R.id.empty_state_container)
    LinearLayout emptyStateContainer;
    @BindView(R.id.text_name_intransaksi)
    TextView textNameIntransaksi;
    @BindView(R.id.button_print_pdf)
    MaterialRippleLayout buttonPrintPdf;
    @BindView(R.id.layout_transaction)
    LinearLayout layoutTransaction;

    private TransactionViewModel transactionViewModel;
    private TransactionReportRvAdapter transactionReportRvAdapter;
    private List<Transaction> transactionArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction);
        ButterKnife.bind(this);

        textNameIntransaksi.setText(QibeeApp.getInstance().getUsername());

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        DividerItemDecoration decoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        rvTransaction.addItemDecoration(decoration);
        transactionReportRvAdapter = new TransactionReportRvAdapter(this);
        rvTransaction.setAdapter(transactionReportRvAdapter);
        rvTransaction.setLayoutManager(layoutManager);
        transactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel.class);
        transactionViewModel.getAllTransactions().observe(this, transactions -> {
            transactionReportRvAdapter.setTransactions(transactions);
            transactionArrayList = transactions;
            if (transactionReportRvAdapter.getItemCount() == 0) {
                layoutTransaction.setVisibility(View.GONE);
                buttonPrintPdf.setVisibility(View.GONE);
                emptyStateContainer.setVisibility(View.VISIBLE);
            } else {
                emptyStateContainer.setVisibility(View.GONE);
            }
        });

        getTotal();
    }

    @SuppressLint("StaticFieldLeak")
    private void getTotal() {
        final int[] totalCashIn = {0};
        final int[] totalCashOut = {0};
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                if (transactionViewModel.getTotalCashIn().getTotal() != 0) {
                    totalCashIn[0] = transactionViewModel.getTotalCashIn().getTotal();
                }

                if (transactionViewModel.getTotalCashOut().getTotal() != 0) {
                    totalCashOut[0] = transactionViewModel.getTotalCashOut().getTotal();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                textTotalIn.setText(Helper.formatRupiah(totalCashIn[0]));
                textTotalOut.setText(Helper.formatRupiah(totalCashOut[0]));
            }
        }.execute();

    }

    private void showPdfDone() {
        new AlertDialog.Builder(TransactionActivity.this, R.style.Theme_AppCompat_DayNight_Dialog_Alert)
                .setMessage("Laporan format PDF selesai dibuat.\n" +
                        "Silakan buka di folder Document")
                .setCancelable(false)
                .setPositiveButton("OK", (dialog, id) -> TransactionActivity.super.onBackPressed())
//                .setNegativeButton("No", null)
                .show();
    }

    @OnClick(R.id.button_print_pdf)
    public void onViewClicked() {
        showPdfDone();
        Intent intent = new Intent(this, PdfActivity.class);
        startActivity(intent);
    }
}
