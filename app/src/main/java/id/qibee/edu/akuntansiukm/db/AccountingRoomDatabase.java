package id.qibee.edu.akuntansiukm.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

@Database(entities = {Product.class, Sales.class, Cart.class, Purchase.class, Expense.class, Transaction.class},
        version = 3, exportSchema = false)
//Schema export directory is not provided to the annotation processor so we cannot export the schema.
// You can either provide `room.schemaLocation` annotation processor argument OR set exportSchema to false.
@TypeConverters(DateTypeConverter.class)
public abstract class AccountingRoomDatabase extends RoomDatabase {

    public abstract CartDao cartDao();
    public abstract PurchaseDao purchaseDao();
    public abstract ExpenseDao expenseDao();
    public abstract SalesDao salesDao();
    public abstract ProductDao productDao();
    public abstract TransactionDao transactionDao();

    private static AccountingRoomDatabase INSTANCE;

    public static AccountingRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AccountingRoomDatabase.class) {
                if (INSTANCE == null) {
                    //create db here
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AccountingRoomDatabase.class, "cart_database")
                            // Wipes and rebuilds instead of migrating if no Migration object.
                            // Migration is not part of this practical.

                            .fallbackToDestructiveMigration()
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    // This callback is called when the database has opened.
    // In this case, use PopulateDbAsync to populate the database
    // with the initial data set if the database has no entries.
    private static Callback sRoomDatabaseCallback =
            new Callback() {
                @Override
                public void onCreate(@NonNull SupportSQLiteDatabase db) {
                    super.onCreate(db);
                }

                @Override
                public void onOpen(@NonNull SupportSQLiteDatabase db) {
                    super.onOpen(db);
                    new PopulateDbAsync(INSTANCE).execute();
                }
            };

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final CartDao cartDao;

        @Override
        protected Void doInBackground(final Void... params) {

            cartDao.getAllCarts();
            return null;
        }

        public PopulateDbAsync(AccountingRoomDatabase db) {
            cartDao = db.cartDao();
        }

    }

    public void clearDb() {
        if (INSTANCE != null) {
            new PopulateDbAsync(INSTANCE).execute();
        }
    }
}
