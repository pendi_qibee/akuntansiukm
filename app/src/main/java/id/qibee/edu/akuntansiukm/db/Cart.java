package id.qibee.edu.akuntansiukm.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import java.util.Date;

@Entity(tableName = "cart_table")
public class Cart {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private int transaction_id;
    private int quantity;
    private int stock;
    private int price;
    private int subTotal;
    private int poductId;
    private String poductName;
    private String notes;

    @ColumnInfo(name = "sales_date")
    @TypeConverters({TimestampConverter.class})
    private Date dateTime;


    @Ignore
    public Cart(int id, int transaction_id, int quantity, int stock, int price, int subTotal, int poductId, String poductName, String notes, Date dateTime) {
        this.id = id;
        this.transaction_id = transaction_id;
        this.quantity = quantity;
        this.stock = stock;
        this.price = price;
        this.subTotal=subTotal;
        this.poductId = poductId;
        this.poductName = poductName;
        this.notes = notes;
        this.dateTime = dateTime;
    }


    public Cart(int transaction_id, int quantity,int stock, int price, int subTotal, int poductId, String poductName, String notes, Date dateTime) {
        this.transaction_id = transaction_id;
        this.quantity = quantity;
        this.stock = stock;
        this.price = price;
        this.subTotal = subTotal;
        this.poductId = poductId;
        this.poductName = poductName;
        this.notes = notes;
        this.dateTime = dateTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(int transaction_id) {
        this.transaction_id = transaction_id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getPrice() {
        return price;
    }

    public int getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(int subTotal) {
        this.subTotal = subTotal;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getPoductId() {
        return poductId;
    }

    public void setPoductId(int poductId) {
        this.poductId = poductId;
    }

    public String getPoductName() {
        return poductName;
    }

    public void setPoductName(String poductName) {
        this.poductName = poductName;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }
}
