package id.qibee.edu.akuntansiukm.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface CartDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Cart cart);

    @Query("DELETE FROM cart_table")
    void deleteAll();

    @Delete
    void deleteCart(Cart cart);

    @Query("SELECT * from cart_table LIMIT 1")
    Cart[] getAnyProduct();

    @Query("SELECT * from cart_table ORDER BY poductName ASC")
    LiveData<List<Cart>> getAllCarts();

    @Update(onConflict = REPLACE)
    void update(Cart... carts);

    @Query("SELECT * from cart_table WHERE transaction_id = :transactionId AND poductId = :productId")
    LiveData<List<Cart>> getProductInserted(int transactionId, int productId);

//    @Query("SELECT COUNT")

    @Query("SELECT * from cart_table WHERE strftime('%m', sales_date) = 06")
    LiveData<List<Cart>> getSalesInMonth();

    @Query("SELECT * from cart_table WHERE transaction_id = :transactionId")
    LiveData<List<Cart>> getCurrentCart(int transactionId);

    @Query("SELECT sum(subTotal) as total from cart_table WHERE transaction_id = :transactionId")
    TotalCash getTotalCart(int transactionId);

    @Query("DELETE from cart_table WHERE transaction_id = :id")
    int deleteCartWithId(int id);

}
