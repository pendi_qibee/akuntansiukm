package id.qibee.edu.akuntansiukm.db;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

public class CartRepository {
    private CartDao mCartDao;
    private LiveData<List<Cart>> mAllSales;
    private LiveData<List<Cart>> mProductInserted;

    public CartRepository(Application application) {
        AccountingRoomDatabase db = AccountingRoomDatabase.getDatabase(application);
        mCartDao = db.cartDao();
        mAllSales = mCartDao.getAllCarts();
//        mProductInserted=mCartDao.getProductInserted(int trId, int prodId);
    }



    public LiveData<List<Cart>> getAllItem() {
        return mAllSales;
    }

    public void insertCart(Cart cart) {
        new insertCartAsyncTask(mCartDao).execute(cart);

    }
//
    public void updateCart(Cart cart) {
        new updateCartAsyncTask(mCartDao).execute(cart);

    }

    public void deleteCart(Cart cart) {
        new deleteCartAsyncTask(mCartDao).execute(cart);

    }

    public void deleteAllCart() {
        new deleteAllCartAsyncTask(mCartDao).execute();

    }

    public LiveData<List<Cart>> getProductInserted(int transId, int prodId){

        new AsyncTask<Void, Void, LiveData<List<Cart>>>(){
            @Override
            protected LiveData<List<Cart>> doInBackground(Void... voids) {
                return mCartDao.getProductInserted(transId, prodId);
            }
        }.execute();
        return null;
    }

    public LiveData<List<Cart>> getCurrentCart(int transactionId) {
        return mCartDao.getCurrentCart(transactionId);
    }

    public TotalCash getTotal(int transactionId) {
        return new getTotalAsyncTask(transactionId).doInBackground();
    }

    public void deleteCartWithId(int id) {
        new deleteCartWithId(id).execute();
    }

    private static class updateCartAsyncTask extends AsyncTask<Cart, Void, Void> {
        private CartDao mAsyncTaskCartDao;

        updateCartAsyncTask(CartDao dao) {
            mAsyncTaskCartDao = dao;
        }

        @Override
        protected Void doInBackground(final Cart... sales) {
            mAsyncTaskCartDao.update(sales);
            return null;
        }
    }

    private static class deleteCartAsyncTask extends AsyncTask<Cart, Void, Void> {
        private CartDao mAsyncTaskCartDao;

        deleteCartAsyncTask(CartDao dao) {
            mAsyncTaskCartDao = dao;
        }

        @Override
        protected Void doInBackground(final Cart... sales) {
            mAsyncTaskCartDao.deleteCart(sales[0]);
            return null;
        }
    }

    private static class deleteAllCartAsyncTask extends AsyncTask<Void, Void, Void> {
        private CartDao cartDao;

        deleteAllCartAsyncTask(CartDao dao) {
            cartDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            cartDao.deleteAll();
            return null;
        }
    }

    private class insertCartAsyncTask extends AsyncTask<Cart, Void, Void>{

        private CartDao cartDao;
        insertCartAsyncTask(CartDao dao) {
            cartDao = dao;
        }

        @Override
        protected Void doInBackground(Cart... sales) {
            cartDao.insert(sales[0]);
            return null;
        }
    }

    private class getProductInsertedAsyncTask extends AsyncTask<Cart, Void, Void>{
        public getProductInsertedAsyncTask(CartDao mCartDao) {
        }

        @Override
        protected Void doInBackground(Cart... carts) {

            return null;
        }
    }

    public class getTotalAsyncTask extends AsyncTask<Void, Void, TotalCash> {
        int transId;

        public getTotalAsyncTask(int transactionId) {
            this.transId=transactionId;
        }

        public int getTotalAsyncTask(CartDao mCartDao, int transactionId) {
            transId=transactionId;
            return transId;
        }

        @Override
        protected TotalCash doInBackground(Void... voids) {
            return mCartDao.getTotalCart(transId);
        }
    }

    private class deleteCartWithId extends AsyncTask<Void, Void, Void>{
        int id;
        public deleteCartWithId(int id) {
            this.id =id;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mCartDao.deleteCartWithId(id);
            return null;
        }
    }
}
