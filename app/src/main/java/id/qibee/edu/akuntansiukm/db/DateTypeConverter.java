package id.qibee.edu.akuntansiukm.db;


import android.arch.persistence.room.TypeConverter;

import java.sql.Date;

public class DateTypeConverter {
//sample take from event apps
    @TypeConverter
    public static Date toDate(Long timestamp) {
//        return timestamp == null ? null : LocalDateTime.ofEpochSecond(timestamp, 0, ZoneOffset.ofTotalSeconds(0));
    return timestamp==null?null: new Date(timestamp);
    }

    @TypeConverter
    public static Long toTimestamp(Date date) {
        //return date == null ? null : date.toInstant(ZoneOffset.ofTotalSeconds(0)).getEpochSecond();
        if (date == null) {
            return null;
        } else {
            return date.getTime();
        }
    }
}

