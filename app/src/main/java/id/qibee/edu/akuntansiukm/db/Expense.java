package id.qibee.edu.akuntansiukm.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import java.util.Date;

@Entity(tableName = "expense_table")
public class Expense {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String transactionCode;
    private int cost;
    private String costName;
    private String notes;


    @ColumnInfo(name = "expense_date")
    @TypeConverters({TimestampConverter.class})
    private Date expenseDate;

    public Expense(String transactionCode, int cost, String costName, String notes, Date expenseDate) {
        this.transactionCode = transactionCode;
        this.cost = cost;
        this.costName = costName;
        this.notes = notes;
        this.expenseDate = expenseDate;
    }


    @Ignore
    public Expense(int id, String transactionCode, int cost, String costName, String notes, Date expenseDate) {
        this.id = id;
        this.transactionCode = transactionCode;
        this.cost = cost;
        this.costName = costName;
        this.notes = notes;
        this.expenseDate = expenseDate;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTransactionCode() {
        return transactionCode;
    }

    public void setTransactionCode(String transactionCode) {
        this.transactionCode = transactionCode;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public String getCostName() {
        return costName;
    }

    public void setCostName(String costName) {
        this.costName = costName;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }


    public Date getExpenseDate() {
        return expenseDate;
    }

    public void setExpenseDate(Date expenseDate) {
        this.expenseDate = expenseDate;
    }
}
