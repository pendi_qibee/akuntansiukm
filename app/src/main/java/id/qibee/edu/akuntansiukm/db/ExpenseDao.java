package id.qibee.edu.akuntansiukm.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface ExpenseDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Expense expense);

    @Query("DELETE FROM expense_table")
    void deleteAll();

    @Delete
    void deleteExpense(Expense expense);

    @Query("SELECT * from expense_table LIMIT 1")
    Expense[] getAnySales();

    @Query("SELECT * from expense_table ORDER BY expense_date ASC")
    LiveData<List<Expense>> getAllExpenses();

    @Update
    void update(Expense... expenses);

    @Query("SELECT * from expense_table ORDER BY id DESC LIMIT 1")
    LiveData<List<Expense>> getMaxId();

//    @Query("SELECT DISTINCT case strftime('%m', date('now')) when '01' then 'Januari' when '02' then 'Febuari' when '03' then 'Maret' when '04' then 'April' when '05' then 'Mei' when '06' then 'Juni' when '07' then 'Juli' when '08' then 'Agustus' when '09' then 'September' when '10' then 'Oktober' when '11' then 'November' when '12' then 'Desember' else '' end as month from purchase_table")
    @Query("SELECT DISTINCT case strftime('%m', expense_date) when '01' then 'Januari' when '02' then 'Febuari' when '03' then 'Maret' when '04' then 'April' when '05' then 'Mei' when '06' then 'Juni' when '07' then 'Juli' when '08' then 'Agustus' when '09' then 'September' when '10' then 'Oktober' when '11' then 'November' when '12' then 'Desember' else '' end as month from expense_table")
    LiveData<List<Month>> getMonth();

    @Query("SELECT sum(cost) as total from expense_table")
    TotalExpense getTotal();

    @Query("SELECT * FROM expense_table WHERE strftime('%m', expense_date) = :monthExpenses")
    LiveData<List<Expense>> getExpenseInMonth(String monthExpenses);

    @Query("SELECT sum(cost) AS total FROM expense_table WHERE strftime('%m', expense_date) = :month")
    TotalExpense getTotalExpensesInMonth(String month);


}
