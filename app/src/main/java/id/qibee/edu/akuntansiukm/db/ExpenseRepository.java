package id.qibee.edu.akuntansiukm.db;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

public class ExpenseRepository {
    private ExpenseDao expenseDao;
    private LiveData<List<Expense>> mAllExpense;

    public ExpenseRepository(Application application) {
        AccountingRoomDatabase db = AccountingRoomDatabase.getDatabase(application);
        expenseDao = db.expenseDao();
        mAllExpense = expenseDao.getAllExpenses();
    }

    public LiveData<List<Expense>> getAllExpenses() {
        return mAllExpense;
    }

    public void insertExpense(Expense expense) {
        new insertExpenseAsyncTask(expenseDao).execute(expense);

    }
//
    public void updateExpense(Expense expense) {
        new updateExpenseAsyncTask(expenseDao).execute(expense);

    }

    public void deleteExpense(Expense expense) {
        new deleteExpensesAsyncTask(expenseDao).execute(expense);

    }

    public void deleteAllExpenses() {
        new deleteAllExpensesAsyncTask(expenseDao).execute();

    }

    public LiveData<List<Month>> getMonth() {
        return new getMonthAsyncTask().doInBackground();
    }

    public LiveData<List<Expense>> getMaxTransId() {
        return expenseDao.getMaxId();
    }

    public TotalExpense getTotal() {
        return new getTotalAsyncTask().doInBackground();
    }

    public LiveData<List<Expense>> getExpensesInMonth(String month) {

        return expenseDao.getExpenseInMonth(month);
    }

    public TotalExpense getTotalInMonth(String month) {
        return new ExpenseRepository.getTotalInMonth(month).doInBackground();
    }

    private static class updateExpenseAsyncTask extends AsyncTask<Expense, Void, Void> {
        private ExpenseDao mAsyncTaskPurchaseDao;

        updateExpenseAsyncTask(ExpenseDao dao) {
            mAsyncTaskPurchaseDao = dao;
        }

        @Override
        protected Void doInBackground(final Expense... expenses) {
            mAsyncTaskPurchaseDao.update(expenses[0]);
            return null;
        }
    }

    private static class deleteExpensesAsyncTask extends AsyncTask<Expense, Void, Void> {
        private ExpenseDao mAsyncTaskExpenseDao;

        deleteExpensesAsyncTask(ExpenseDao dao) {
            mAsyncTaskExpenseDao = dao;
        }

        @Override
        protected Void doInBackground(final Expense... Expense) {
            mAsyncTaskExpenseDao.deleteExpense(Expense[0]);
            return null;
        }
    }

    private static class deleteAllExpensesAsyncTask extends AsyncTask<Void, Void, Void> {
        private ExpenseDao expenseDao1;

        deleteAllExpensesAsyncTask(ExpenseDao dao) {
            expenseDao1 = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            expenseDao1.deleteAll();
            return null;
        }
    }

    private class insertExpenseAsyncTask extends AsyncTask<Expense, Void, Void>{

        private ExpenseDao expenseDao1;
        insertExpenseAsyncTask(ExpenseDao dao) {
            expenseDao1 = dao;
        }

        @Override
        protected Void doInBackground(Expense... Expense) {
            expenseDao1.insert(Expense[0]);
            return null;
        }
    }

    private class getMonthAsyncTask extends AsyncTask<Void, Void, LiveData<List<Month>>> {

        public getMonthAsyncTask() {

        }

        @Override
        protected LiveData<List<Month>> doInBackground(Void... voids) {
            return expenseDao.getMonth();
        }
    }

    public class getTotalAsyncTask extends AsyncTask<Void, Void, TotalExpense> {
private ExpenseDao expenseDaoInAsync;


        @Override
        protected TotalExpense doInBackground(Void... voids) {
            return expenseDao.getTotal();
        }
    }

    private class getTotalInMonth extends AsyncTask<Void, Void, TotalExpense> {
        String month;

        public getTotalInMonth(String month) {
            this.month = month;
        }

        @Override
        protected TotalExpense doInBackground(Void... voids) {
            return expenseDao.getTotalExpensesInMonth(month);
        }
    }

}
