package id.qibee.edu.akuntansiukm.db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "product_table")
public class Product {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private int stock;
    private String name;
    private String unit;
    private int price;

    @Ignore
    public Product(int id, int stock, String name, String unit, int price) {
        this.id = id;
        this.stock = stock;
        this.name = name;
        this.unit = unit;
        this.price = price;
    }

    public Product(int stock, String name, String unit, int price) {
        this.stock = stock;
        this.name = name;
        this.unit = unit;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
