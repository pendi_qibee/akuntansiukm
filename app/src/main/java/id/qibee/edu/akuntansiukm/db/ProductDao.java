package id.qibee.edu.akuntansiukm.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface ProductDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Product product);

    @Query("DELETE FROM product_table")
    void deleteAll();

    @Delete
    void deleteProduct(Product Product);

    @Query("SELECT * from product_table LIMIT 1")
    Product[] getAnyProduct();

    @Query("SELECT * from product_table ORDER BY name ASC")
    LiveData<List<Product>> getAllProducts();

    @Update
    void update(Product... products);


    @Query("SELECT stock from product_table WHERE id = :idProduct")
    int getStock(int idProduct);

    @Query("SELECT * from product_table WHERE id = :idProduct")
    Product getDataWithId(int idProduct);

    @Query("UPDATE product_table SET stock = :stockUpdate  WHERE id = :idProduct")
    int updateStockWithId(int idProduct, int[] stockUpdate);
}
