package id.qibee.edu.akuntansiukm.db;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;
import android.util.Log;

import java.util.List;

public class ProductRepository {
    private ProductDao mProductDao;
    private LiveData<List<Product>> mAllProducts;
    private Product product;
/*
    public ProductRepository(ProductDao mProductDao, LiveData<List<Product>> mAllProducts) {
        this.mProductDao = mProductDao;
        this.mAllProducts = mAllProducts;
    }*/

    public ProductRepository(Application application) {
        AccountingRoomDatabase db = AccountingRoomDatabase.getDatabase(application);
        mProductDao = db.productDao();
        mAllProducts = mProductDao.getAllProducts();
//        product=mProductDao.getDataWithId();
    }

    public LiveData<List<Product>> getAllProduct() {
        return mAllProducts;
    }

    public void insertProduct(Product product) {
        new insertAsyncTask(mProductDao).execute(product);

    }

    public void updateProduct(Product product) {
        new updateAsyncTask(mProductDao).execute(product);

    }

    public void updateStock(int productId, int[] stockUpdate) {
        new AsyncTask<Void, Void, Integer>(){
            @Override
            protected Integer doInBackground(Void... voids) {
                return mProductDao.updateStockWithId(productId, stockUpdate);
            }
        }.execute();

    }

    public void deleteProduct(Product product) {
        new deleteProductAsyncTask(mProductDao).execute(product);

    }

    public void deleteAllProduct() {
        new deleteAllProductAsyncTask(mProductDao).execute();

    }

    public int getStock(int id) {
        return new getStockAsyncTask(id).doInBackground();


    }

    public LiveData<Product> getDataWithId(int id) {
        final MutableLiveData<Product> productMutableLiveData = new MutableLiveData<>();
        new AsyncTask<Void, Void, Product>() {
            @Override
            protected Product doInBackground(Void... voids) {
                return mProductDao.getDataWithId(id);
            }

            @Override
            protected void onPostExecute(Product product) {
                super.onPostExecute(product);
                productMutableLiveData.setValue(product);
            }
        }.execute();
        return productMutableLiveData;
    }



    private class insertAsyncTask extends AsyncTask<Product, Void, Void> {

        private ProductDao mAsyncProductDao;

        insertAsyncTask(ProductDao dao) {
            mAsyncProductDao = dao;
        }

        @Override
        protected Void doInBackground(final Product... products) {
            mAsyncProductDao.insert(products[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.d("", "onPostExecute: berhasil simpan data");
        }
    }

    private static class updateAsyncTask extends android.os.AsyncTask<Product, Void, Void> {
        private ProductDao mAsyncTaskProductDao;

        updateAsyncTask(ProductDao dao) {
            mAsyncTaskProductDao = dao;
        }

        @Override
        protected Void doInBackground(final Product... products) {
            mAsyncTaskProductDao.update(products[0]);
            return null;
        }
    }

    private static class deleteProductAsyncTask extends AsyncTask<Product, Void, Void> {
        private ProductDao mAsyncTaskProductDao;

        deleteProductAsyncTask(ProductDao dao) {
            mAsyncTaskProductDao = dao;
        }

        @Override
        protected Void doInBackground(final Product... products) {
            mAsyncTaskProductDao.deleteProduct(products[0]);
            return null;
        }
    }

    private static class deleteAllProductAsyncTask extends AsyncTask<Void, Void, Void> {
        private ProductDao mAsyncTaskProductDao;

        deleteAllProductAsyncTask(ProductDao dao) {
            mAsyncTaskProductDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mAsyncTaskProductDao.deleteAll();
            return null;
        }
    }

    private class getStockAsyncTask extends AsyncTask<Void, Void, Integer> {
        private ProductDao mAsyncProductDao;
        int id;
        int stock;

        public getStockAsyncTask(int id) {
            this.id = id;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            return mAsyncProductDao.getStock(id);
        }
    }

//failed
//    private class getDataAsyncTask extends AsyncTask<Integer, Void, Product> {
//        int idProduct;
//        private ProductDao productDao;
//        public getDataAsyncTask(ProductDao dao) {
//            productDao = dao;
//        }
//
//
//
//
//        @Override
//        protected Product doInBackground(Integer... integers) {
//            productDao.getDataWithId(integers);
//            return null;
//        }
//    }
}
