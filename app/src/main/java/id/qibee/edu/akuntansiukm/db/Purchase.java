package id.qibee.edu.akuntansiukm.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import java.util.Date;

@Entity(tableName = "purchase_table")
public class Purchase {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String transactionCode;
    private int totalCash;
    private String allProductName;
    private String notes;
    private String vendorName;

    @ColumnInfo(name = "purchase_date")
    @TypeConverters({TimestampConverter.class})
    private Date purchaseDate;

    public Purchase(String transactionCode, int totalCash, String allProductName, String notes, String vendorName, Date purchaseDate) {
        this.transactionCode = transactionCode;
        this.totalCash = totalCash;
        this.allProductName = allProductName;
        this.notes = notes;
        this.vendorName = vendorName;
        this.purchaseDate = purchaseDate;
    }

    @Ignore
    public Purchase(int id, String transactionCode, int totalCash, String allProductName, String notes, String vendorName, Date purchaseDate) {
        this.id = id;
        this.transactionCode = transactionCode;
        this.totalCash = totalCash;
        this.allProductName = allProductName;
        this.notes = notes;
        this.vendorName = vendorName;
        this.purchaseDate = purchaseDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTransactionCode() {
        return transactionCode;
    }

    public void setTransactionCode(String transactionCode) {
        this.transactionCode = transactionCode;
    }

    public int getTotalCash() {
        return totalCash;
    }

    public void setTotalCash(int totalCash) {
        this.totalCash = totalCash;
    }

    public String getAllProductName() {
        return allProductName;
    }

    public void setAllProductName(String allProductName) {
        this.allProductName = allProductName;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }
}
