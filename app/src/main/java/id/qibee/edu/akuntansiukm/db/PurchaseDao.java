package id.qibee.edu.akuntansiukm.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface PurchaseDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Purchase purchase);

    @Query("DELETE FROM purchase_table")
    void deleteAll();

    @Delete
    void deletePurchases(Purchase purchase);

    @Query("SELECT * from purchase_table LIMIT 1")
    Purchase[] getAnySales();

    @Query("SELECT * from purchase_table ORDER BY purchase_date ASC")
    LiveData<List<Purchase>> getAllPurchases();

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(Purchase... purchases);

    @Query("SELECT * from purchase_table ORDER BY id DESC LIMIT 1")
    LiveData<List<Purchase>> getMaxId();

//    @Query("SELECT DISTINCT case strftime('%m', date('now')) when '01' then 'Januari' when '02' then 'Febuari' when '03' then 'Maret' when '04' then 'April' when '05' then 'Mei' when '06' then 'Juni' when '07' then 'Juli' when '08' then 'Agustus' when '09' then 'September' when '10' then 'Oktober' when '11' then 'November' when '12' then 'Desember' else '' end as month from purchase_table")
    @Query("SELECT DISTINCT case strftime('%m', purchase_date) when '01' then 'Januari' when '02' then 'Febuari' when '03' then 'Maret' when '04' then 'April' when '05' then 'Mei' when '06' then 'Juni' when '07' then 'Juli' when '08' then 'Agustus' when '09' then 'September' when '10' then 'Oktober' when '11' then 'November' when '12' then 'Desember' else '' end as month from purchase_table")
    LiveData<List<Month>> getMonth();

    @Query("SELECT sum(totalCash) as total from purchase_table")
    TotalPurchase getTotal();

    @Query("SELECT sum(totalCash) AS total FROM purchase_table WHERE strftime('%m', purchase_date) = :month")
    TotalPurchase getTotalPurchasesInMonth(String month);

    @Query("SELECT * FROM purchase_table WHERE strftime('%m', purchase_date) = :monthPurchases")
    LiveData<List<Purchase>> getPurchasesInMonth(String monthPurchases);
}
