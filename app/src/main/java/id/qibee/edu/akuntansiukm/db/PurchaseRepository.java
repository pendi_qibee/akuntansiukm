package id.qibee.edu.akuntansiukm.db;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

public class PurchaseRepository {
    private PurchaseDao mPurchaseDao;
    private LiveData<List<Purchase>> mAllSales;

    public PurchaseRepository(Application application) {
        AccountingRoomDatabase db = AccountingRoomDatabase.getDatabase(application);
        mPurchaseDao = db.purchaseDao();
        mAllSales = mPurchaseDao.getAllPurchases();
    }

    public LiveData<List<Purchase>> getAllPurchases() {
        return mAllSales;
    }

    public void insertPurchase(Purchase purchase) {
        new insertPurchaseAsyncTask(mPurchaseDao).execute(purchase);

    }
//
    public void updatePurchase(Purchase purchase) {
        new updateSalesAsyncTask(mPurchaseDao).execute(purchase);

    }

    public void deletePurchase(Purchase purchase) {
        new deleteSalesAsyncTask(mPurchaseDao).execute(purchase);

    }

    public void deleteAllPurchases() {
        new deleteAllSalesAsyncTask(mPurchaseDao).execute();

    }

    public LiveData<List<Month>> getMonth() {
        return new getMonthAsyncTask().doInBackground();
    }

    public LiveData<List<Purchase>> getPurchasesInMonth(String month) {

        return mPurchaseDao.getPurchasesInMonth(month);

    }

    public LiveData<List<Purchase>> getMaxTransId() {
        return mPurchaseDao.getMaxId();
    }

    public TotalPurchase getTotal() {
        return new getTotalAsyncTask().doInBackground();
    }

    public TotalPurchase getTotalInMonth(String month) {
        return new PurchaseRepository.getTotalInMonth(month).doInBackground();
    }

    private static class updateSalesAsyncTask extends AsyncTask<Purchase, Void, Void> {
        private PurchaseDao mAsyncTaskPurchaseDao;

        updateSalesAsyncTask(PurchaseDao dao) {
            mAsyncTaskPurchaseDao = dao;
        }

        @Override
        protected Void doInBackground(final Purchase... purchases) {
            mAsyncTaskPurchaseDao.update(purchases[0]);
            return null;
        }
    }

    private static class deleteSalesAsyncTask extends AsyncTask<Purchase, Void, Void> {
        private PurchaseDao mAsyncTaskSalesDao;

        deleteSalesAsyncTask(PurchaseDao dao) {
            mAsyncTaskSalesDao = dao;
        }

        @Override
        protected Void doInBackground(final Purchase... Purchase) {
            mAsyncTaskSalesDao.deletePurchases(Purchase[0]);
            return null;
        }
    }

    private static class deleteAllSalesAsyncTask extends AsyncTask<Void, Void, Void> {
        private PurchaseDao purchaseDao;

        deleteAllSalesAsyncTask(PurchaseDao dao) {
            purchaseDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            purchaseDao.deleteAll();
            return null;
        }
    }

    private class insertPurchaseAsyncTask extends AsyncTask<Purchase, Void, Void>{

        private PurchaseDao purchaseDao;
        insertPurchaseAsyncTask(PurchaseDao dao) {
            purchaseDao = dao;
        }

        @Override
        protected Void doInBackground(Purchase... Purchase) {
            purchaseDao.insert(Purchase[0]);
            return null;
        }
    }

    private class getMonthAsyncTask extends AsyncTask<Void, Void, LiveData<List<Month>>> {

        public getMonthAsyncTask() {

        }

        @Override
        protected LiveData<List<Month>> doInBackground(Void... voids) {
            return mPurchaseDao.getMonth();
        }
    }

    public class getTotalAsyncTask extends AsyncTask<Void, Void, TotalPurchase> {
        private ExpenseDao expenseDaoInAsync;


        @Override
        protected TotalPurchase doInBackground(Void... voids) {
            return mPurchaseDao.getTotal();
        }
    }

    private class getTotalInMonth extends AsyncTask<Void, Void, TotalPurchase> {
        String month;

        public getTotalInMonth(String month) {
            this.month = month;
        }

        @Override
        protected TotalPurchase doInBackground(Void... voids) {
            return mPurchaseDao.getTotalPurchasesInMonth(month);
        }
    }
}
