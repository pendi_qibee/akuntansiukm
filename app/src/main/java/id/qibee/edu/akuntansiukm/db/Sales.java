package id.qibee.edu.akuntansiukm.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import java.util.Date;

@Entity(tableName = "sales_table")
public class Sales {

//    @PrimaryKey(autoGenerate = true)
    @PrimaryKey
    private int id;
    private String transactionCode;
    private String productSold;
    private int totalCash;

    @ColumnInfo(name = "sales_date")
    @TypeConverters({TimestampConverter.class})
    private Date dateTime;

    public Sales(int id, String transactionCode, String productSold, int totalCash, Date dateTime) {
        this.id = id;
        this.transactionCode = transactionCode;
        this.productSold = productSold;
        this.totalCash = totalCash;
        this.dateTime = dateTime;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTransactionCode() {
        return transactionCode;
    }

    public void setTransactionCode(String transactionCode) {
        this.transactionCode = transactionCode;
    }

    public String getProductSold() {
        return productSold;
    }

    public void setProductSold(String productSold) {
        this.productSold = productSold;
    }

    public int getTotalCash() {
        return totalCash;
    }

    public void setTotalCash(int totalCash) {
        this.totalCash = totalCash;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }
}
