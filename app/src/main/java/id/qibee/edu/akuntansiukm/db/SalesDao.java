package id.qibee.edu.akuntansiukm.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface SalesDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Sales Sales);

    @Query("DELETE FROM sales_table")
    void deleteAll();

    @Delete
    void deleteSales(Sales Sales);

    @Query("SELECT * FROM sales_table LIMIT 1")
    Sales[] getAnySales();

    @Query("SELECT * FROM sales_table ORDER BY sales_date ASC")
    LiveData<List<Sales>> getAllSaless();

    @Update
    void update(Sales... Saless);

    @Query("SELECT * FROM sales_table ORDER BY id DESC LIMIT 1")
    LiveData<List<Sales>> getMaxId();

//    @Query("SELECT DISTINCT case strftime('%m', date('now')) when '01' then 'Januari' when '02' then 'Febuari' when '03' then 'Maret' when '04' then 'April' when '05' then 'Mei' when '06' then 'Juni' when '07' then 'Juli' when '08' then 'Agustus' when '09' then 'September' when '10' then 'Oktober' when '11' then 'November' when '12' then 'Desember' else '' end as month from sales_table")
    @Query("SELECT DISTINCT case strftime('%m', sales_date) when '01' then 'JANUARI' when '02' then 'FEBRUARI' when '03' then 'MARET' when '04' then 'APRIL' when '05' then 'MEI' when '06' then 'JUNI' when '07' then 'JULI' when '08' then 'AGUSTUS' when '09' then 'SEPTEMBER' when '10' then 'OKTOBER' when '11' then 'NOVEMBER' when '12' then 'DESEMBER' else '' end as month from sales_table ORDER BY strftime('%m', sales_date) DESC")
    LiveData<List<Month>> getMonth();

    @Query("SELECT sum(totalCash) AS total FROM sales_table")
    TotalSales getTotal();

    @Query("SELECT sum(totalCash) AS total FROM sales_table WHERE strftime('%m', sales_date) = :month")
    TotalSales getTotalSalesInMonth(String month);

    @Query("SELECT * FROM sales_table WHERE strftime('%m', sales_date) = :monthString")
    LiveData<List<Sales>> getSalesInMonth(String monthString);

//    @Query("SELECT * FROM sales_table WHERE sales_date  BETWEEN :startDate AND :endDate")
//    LiveData<List<SalesModel>> getTransactionsInMonth(Date startDate, Date endDate);

//    @Query("SELECT * FROM sales_table WHERE strftime('%m', sales_date) = :monthSales")
//    LiveData<List<SalesModel>> getTransactionsInMonth(String monthSales);

//    @Query("SELECT * FROM sales_table WHERE sales_date LIKE :monthSales")
//    LiveData<List<SalesModel>> getTransactionsInMonth(String monthSales);


    //failed
//    error: There is a problem with the query: [SQLITE_ERROR]
//    SQL error or missing database (no such function: MONTH)
//    @Query("SELECT * FROM sales_table WHERE MONTH(sales_date) = :monthSales")
//    LiveData<List<SalesModel>> getTransactionsInMonth(String monthSales);

}
