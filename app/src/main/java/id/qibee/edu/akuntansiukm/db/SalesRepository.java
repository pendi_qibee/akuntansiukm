package id.qibee.edu.akuntansiukm.db;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

public class SalesRepository {
    private SalesDao mSalesDao;
    private LiveData<List<Sales>> mAllSales;

    public SalesRepository(Application application) {
        AccountingRoomDatabase db = AccountingRoomDatabase.getDatabase(application);
        mSalesDao = db.salesDao();
        mAllSales = mSalesDao.getAllSaless();
    }

    public LiveData<List<Sales>> getAllSales() {
        return mAllSales;
    }

    public void insertSales(Sales Sales) {
        new insertSalesAsyncTask(mSalesDao).execute(Sales);

    }

    //
    public void updateSales(Sales Sales) {
        new updateSalesAsyncTask(mSalesDao).execute(Sales);

    }

    public void deleteSales(Sales Sales) {
        new deleteSalesAsyncTask(mSalesDao).execute(Sales);

    }

    public void deleteAllSales() {
        new deleteAllSalesAsyncTask(mSalesDao).execute();

    }

    public LiveData<List<Month>> getMonth() {
        return new getMonthAsyncTask().doInBackground();
    }

    public LiveData<List<Sales>> getSalesInMonth(String month) {

        return mSalesDao.getSalesInMonth(month);

    }

    public LiveData<List<Sales>> getMaxTransId() {
        return mSalesDao.getMaxId();
    }

    public TotalSales getTotal() {
        return new getTotalAsyncTask().doInBackground();
    }

    public TotalSales getTotalInMonth(String month) {
        return new getTotalInMonth(month).doInBackground();
    }


    private static class updateSalesAsyncTask extends AsyncTask<Sales, Void, Void> {
        private SalesDao mAsyncTaskSalesDao;

        updateSalesAsyncTask(SalesDao dao) {
            mAsyncTaskSalesDao = dao;
        }

        @Override
        protected Void doInBackground(final Sales... sales) {
            mAsyncTaskSalesDao.update(sales[0]);
            return null;
        }
    }

    private static class deleteSalesAsyncTask extends AsyncTask<Sales, Void, Void> {
        private SalesDao mAsyncTaskSalesDao;

        deleteSalesAsyncTask(SalesDao dao) {
            mAsyncTaskSalesDao = dao;
        }

        @Override
        protected Void doInBackground(final Sales... sales) {
            mAsyncTaskSalesDao.deleteSales(sales[0]);
            return null;
        }
    }

    private static class deleteAllSalesAsyncTask extends AsyncTask<Void, Void, Void> {
        private SalesDao SalesDao;

        deleteAllSalesAsyncTask(SalesDao dao) {
            SalesDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            SalesDao.deleteAll();
            return null;
        }
    }

    private class insertSalesAsyncTask extends AsyncTask<Sales, Void, Void> {

        private SalesDao SalesDao;

        insertSalesAsyncTask(SalesDao dao) {
            SalesDao = dao;
        }

        @Override
        protected Void doInBackground(Sales... sales) {
            SalesDao.insert(sales[0]);
            return null;
        }
    }

    private class getMonthAsyncTask extends AsyncTask<Void, Void, LiveData<List<Month>>> {

        public getMonthAsyncTask() {

        }

        @Override
        protected LiveData<List<Month>> doInBackground(Void... voids) {
            return mSalesDao.getMonth();
        }
    }

    /*private class getMaxAsyncTask extends AsyncTask<SalesModel, Void, Void>{
        private SalesDao cartDao;

        public getMaxAsyncTask() {
        }

        @Override
        protected Void doInBackground(SalesModel... sales) {
            cartDao.getMaxTransactionId();
            return null;
        }
    }*/

    public class getTotalAsyncTask extends AsyncTask<Void, Void, TotalSales> {

        @Override
        protected TotalSales doInBackground(Void... voids) {
            return mSalesDao.getTotal();
        }
    }

    private class getTotalInMonth extends AsyncTask<Void, Void, TotalSales> {
        String month;

        public getTotalInMonth(String month) {
            this.month = month;
        }

        @Override
        protected TotalSales doInBackground(Void... voids) {
            return mSalesDao.getTotalSalesInMonth(month);
        }
    }
}
