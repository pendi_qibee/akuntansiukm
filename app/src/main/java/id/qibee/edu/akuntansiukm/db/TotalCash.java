package id.qibee.edu.akuntansiukm.db;

public class TotalCash {

    public int total;

    public TotalCash(int total) {
        this.total = total;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
