package id.qibee.edu.akuntansiukm.db;

public class TotalExpense {

    public int total;

    public TotalExpense(int total) {
        this.total = total;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
