package id.qibee.edu.akuntansiukm.db;

public class TotalPurchase {

    public int total;

    public TotalPurchase(int total) {
        this.total = total;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
