package id.qibee.edu.akuntansiukm.db;

public class TotalSales {

    public int total;

    public TotalSales(int total) {
        this.total = total;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
