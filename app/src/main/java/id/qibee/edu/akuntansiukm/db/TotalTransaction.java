package id.qibee.edu.akuntansiukm.db;

public class TotalTransaction {

    public int total;

    public TotalTransaction(int total) {
        this.total = total;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
