package id.qibee.edu.akuntansiukm.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import java.util.Date;

@Entity(tableName = "transaction_table")
public class Transaction {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String transactionCode;
    private String type;
    private String content;
    private int cashIn;
    private int cashOut;

    @ColumnInfo(name = "transaction_date")
    @TypeConverters({TimestampConverter.class})
    private Date date;

    @Ignore
    public Transaction(int id, String transactionCode, String type, String content, int cashIn, int cashOut, Date date) {
        this.id = id;
        this.transactionCode = transactionCode;
        this.type = type;
        this.content = content;
        this.cashIn = cashIn;
        this.cashOut = cashOut;
        this.date = date;
    }

    public Transaction(String transactionCode, String type, String content, int cashIn, int cashOut, Date date) {
        this.transactionCode = transactionCode;
        this.type = type;
        this.content = content;
        this.cashIn = cashIn;
        this.cashOut = cashOut;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTransactionCode() {
        return transactionCode;
    }

    public void setTransactionCode(String transactionCode) {
        this.transactionCode = transactionCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getCashIn() {
        return cashIn;
    }

    public void setCashIn(int cashIn) {
        this.cashIn = cashIn;
    }

    public int getCashOut() {
        return cashOut;
    }

    public void setCashOut(int cashOut) {
        this.cashOut = cashOut;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
