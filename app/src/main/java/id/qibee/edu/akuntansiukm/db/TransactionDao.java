package id.qibee.edu.akuntansiukm.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface TransactionDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Transaction cart);

    @Query("DELETE FROM transaction_table")
    void deleteAll();

    @Delete
    void deleteTransaction(Transaction transaction);

    @Query("SELECT * from transaction_table LIMIT 1")
    Transaction[] getAnyProduct();

    @Query("SELECT * from transaction_table ORDER BY transaction_date DESC")
    LiveData<List<Transaction>> getAllTransactions();

    @Update(onConflict = REPLACE)
    void update(Transaction... transactions);

//    @Query("UPDATE transaction_table  SET type =  WHERE transactionCode = :transactionCode")
//    void updateByCode(String transactionCode);


    @Query("SELECT * from transaction_table WHERE strftime('%m', transaction_date) = 06")
    LiveData<List<Transaction>> getTransactionInMonth();

    @Query("SELECT * from transaction_table WHERE id = :transactionId")
    LiveData<List<Transaction>> getCurrentCart(int transactionId);


    @Query("DELETE from transaction_table WHERE id = :id")
    int deleteTransactionWithId(int id);

    @Query("DELETE from transaction_table WHERE transactionCode = :transactionCode")
    void deleteByTransactionCode(String transactionCode);

    @Query("SELECT DISTINCT case strftime('%m', transaction_date) when '01' then 'JANUARI' when '02' then 'FEBRUARI' when '03' then 'MARET' when '04' then 'APRIL' when '05' then 'MEI' when '06' then 'JUNI' when '07' then 'JULI' when '08' then 'AGUSTUS' when '09' then 'SEPTEMBER' when '10' then 'OKTOBER' when '11' then 'NOVEMBER' when '12' then 'DESEMBER' else '' end as month from transaction_table ORDER BY strftime('%m', transaction_date) DESC")
    LiveData<List<Month>> getMonth();

    @Query("SELECT sum(cashIn) AS total FROM transaction_table")
    TotalTransaction getTotalCashIn();
    
    @Query("SELECT sum(cashOut) AS total FROM transaction_table")
    TotalTransaction getTotalCashOut();

    @Query("SELECT sum(cashIn) AS total FROM transaction_table WHERE strftime('%m', transaction_date) = :month")
    TotalTransaction getTotalCashInMonth(String month);
    
    @Query("SELECT sum(cashOut) AS total FROM transaction_table WHERE strftime('%m', transaction_date) = :month")
    TotalTransaction getTotalCashOutMonth(String month);

    @Query("SELECT * FROM transaction_table WHERE strftime('%m', transaction_date) = :monthString")
    LiveData<List<Transaction>> getTransactionInMonth(String monthString);

}
