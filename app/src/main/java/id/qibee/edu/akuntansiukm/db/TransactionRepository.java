package id.qibee.edu.akuntansiukm.db;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

public class TransactionRepository {
    private TransactionDao mTransactionDao;
    private LiveData<List<Transaction>> mAllSales;

    public TransactionRepository(Application application) {
        AccountingRoomDatabase db = AccountingRoomDatabase.getDatabase(application);
        mTransactionDao = db.transactionDao();
        mAllSales = mTransactionDao.getAllTransactions();
    }

    public LiveData<List<Transaction>> getAllTransactions() {
        return mAllSales;
    }

    public void insertTransaction(Transaction Transaction) {
        new insertTransactionsAsyncTask(mTransactionDao).execute(Transaction);

    }

    //
    public void updateTransaction(Transaction Transaction) {
        new updateTransactionsAsyncTask(mTransactionDao).execute(Transaction);

    }
/*
    public void updateByCode(String transactionCode) {
        new updateTransByCode(mTransactionDao).execute(transactionCode);

    }*/

    public void deleteTransaction(Transaction Transaction) {
        new deleteTransactionsAsyncTask(mTransactionDao).execute(Transaction);

    }

    public void deleteByTransactionCode(String code) {
        new deleteTransactionByCodeAsyncTask(mTransactionDao).execute(code);

    }

    public void deleteAllTransactions() {
        new deleteAllTransactionsAsyncTask(mTransactionDao).execute();

    }

    public LiveData<List<Month>> getMonth() {
        return new getMonthAsyncTask().doInBackground();
    }

    public LiveData<List<Transaction>> getTransactionsInMonth(String month) {

        return mTransactionDao.getTransactionInMonth(month);

    }

    public TotalTransaction getTotalCashIn() {
        return new getTotalCashInAsyncTask().doInBackground();
    }

    public TotalTransaction getTotalCashOut() {
        return new getTotalCashOutAsyncTask().doInBackground();
    }

//    public TotalTransaction getTotalInMonth(String month) {
//        return new getTotalInMonth(month).doInBackground();
//    }


    private static class updateTransactionsAsyncTask extends AsyncTask<Transaction, Void, Void> {
        private TransactionDao mAsyncTaskSalesDao;

        updateTransactionsAsyncTask(TransactionDao dao) {
            mAsyncTaskSalesDao = dao;
        }

        @Override
        protected Void doInBackground(final Transaction... transactions) {
            mAsyncTaskSalesDao.update(transactions[0]);
            return null;
        }
    }

//    private static class updateTransByCode extends AsyncTask<String, Void, Void> {
//        private TransactionDao mAsyncTaskSalesDao;
//
//        updateTransByCode(TransactionDao dao) {
//            mAsyncTaskSalesDao = dao;
//        }
//
//        @Override
//        protected Void doInBackground(final String... code) {
//            mAsyncTaskSalesDao.updateByCode(code[0]);
//            return null;
//        }
//    }

    private static class deleteTransactionsAsyncTask extends AsyncTask<Transaction, Void, Void> {
        private TransactionDao mAsyncTaskTransactionDao;

        deleteTransactionsAsyncTask(TransactionDao dao) {
            mAsyncTaskTransactionDao = dao;
        }

        @Override
        protected Void doInBackground(final Transaction... transactions) {
            mAsyncTaskTransactionDao.deleteTransaction(transactions[0]);
            return null;
        }
    }

    private static class deleteAllTransactionsAsyncTask extends AsyncTask<Void, Void, Void> {
        private TransactionDao TransactionDao;

        deleteAllTransactionsAsyncTask(TransactionDao dao) {
            TransactionDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            TransactionDao.deleteAll();
            return null;
        }
    }

    private class insertTransactionsAsyncTask extends AsyncTask<Transaction, Void, Void> {

        private TransactionDao TransactionDao;

        insertTransactionsAsyncTask(TransactionDao dao) {
            TransactionDao = dao;
        }

        @Override
        protected Void doInBackground(Transaction... transactions) {
            TransactionDao.insert(transactions[0]);
            return null;
        }
    }

    private class getMonthAsyncTask extends AsyncTask<Void, Void, LiveData<List<Month>>> {

        public getMonthAsyncTask() {

        }

        @Override
        protected LiveData<List<Month>> doInBackground(Void... voids) {
            return mTransactionDao.getMonth();
        }
    }

    public class getTotalCashInAsyncTask extends AsyncTask<Void, Void, TotalTransaction> {

        @Override
        protected TotalTransaction doInBackground(Void... voids) {
            return mTransactionDao.getTotalCashIn();
        }
    }

    public class getTotalCashOutAsyncTask extends AsyncTask<Void, Void, TotalTransaction> {

        @Override
        protected TotalTransaction doInBackground(Void... voids) {
            return mTransactionDao.getTotalCashOut();
        }
    }

    private class deleteTransactionByCodeAsyncTask extends AsyncTask<String, Void, Void> {
        TransactionDao transactionDao;
        public deleteTransactionByCodeAsyncTask(TransactionDao mTransactionDao) {
            this.transactionDao = mTransactionDao;
        }



        @Override
        protected Void doInBackground(String... strings) {
            transactionDao.deleteByTransactionCode(strings[0]);
            return null;
        }
    }
}
