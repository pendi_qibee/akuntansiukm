package id.qibee.edu.akuntansiukm.util;

/**
 * Created by brijesh on 28/10/17.
 */

public interface Constants {
    String TIME_STAMP_FORMAT = "yyyy-MM-dd HH:mm:ss";
    String DOB_FORMAT = "yyyy-MM-dd";
    String TAG = "debug";
    String AKUNTUKM_PREF = "preference";
    String USERNAME = "user_name";
    String USERFIELD = "user_field";
}
