package id.qibee.edu.akuntansiukm.util;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Helper {

    public static String formatRupiah(int nominal) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        return formatter.format(nominal);
    }

    public static Date currentDate() {
        Date c = Calendar.getInstance().getTime();
        return c;
    }


    public static Date getDateFromString(String dateString) {
        Date dateResult = null;
        SimpleDateFormat timestamp = new SimpleDateFormat(Constants.TIME_STAMP_FORMAT);
        try {
            dateResult = timestamp.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateResult;
    }

    public static String getStringFromDate(Date date) {
        String dateString;
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        dateString = df.format(date);

        return dateString;
    }

    public static Date getDateFromDatePicker(int year, int monthOfYear, int dayOfMonth) {
        Date dateFromPicker;
        Calendar cal = Calendar.getInstance();
        cal.set(year, monthOfYear, dayOfMonth);
        SimpleDateFormat timestamp = new SimpleDateFormat(Constants.TIME_STAMP_FORMAT);
        String formattedDate = timestamp.format(cal.getTime());
        try {
            dateFromPicker = timestamp.parse(formattedDate);
            return dateFromPicker;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

}
