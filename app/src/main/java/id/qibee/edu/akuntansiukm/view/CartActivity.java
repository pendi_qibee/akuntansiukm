package id.qibee.edu.akuntansiukm.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.qibee.edu.akuntansiukm.R;
import id.qibee.edu.akuntansiukm.db.Cart;
import id.qibee.edu.akuntansiukm.db.Product;
import id.qibee.edu.akuntansiukm.viewmodel.CartViewModel;
import id.qibee.edu.akuntansiukm.viewmodel.ProductViewModel;

import static id.qibee.edu.akuntansiukm.view.SalesInputActivity.EXTRA_DATE;
import static id.qibee.edu.akuntansiukm.view.SalesInputActivity.EXTRA_TRANSACTION_ID;

public class CartActivity extends AppCompatActivity {
    private static final String TAG = "Cartlog";
    //    @BindView(R.id.fab_add_sales)
//    FloatingActionButton fabAddSales;
    @BindView(R.id.rv_product_cart)
    RecyclerView rvProductCart;
    @BindView(R.id.empty_state_container)
    LinearLayout emptyStateContainer;
    @BindView(R.id.fab_back)
    FloatingActionButton fabBack;

    private CartViewModel cartViewModel;
    private ProductViewModel productViewModel;
    private int quantity = 1;
    private int transactionId;
    private List<Cart> cartList;
    private List<Product> productList;
    private Product product;
    private ProductCartRvAdapter productCartRvAdapter;
    private boolean isProductInCart;
    private ProductCartRvAdapter.ClickListener clickListener;
private Date salesDate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Stetho.initializeWithDefaults(this);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);
        cartViewModel = ViewModelProviders.of(this).get(CartViewModel.class);
        productViewModel = ViewModelProviders.of(this).get(ProductViewModel.class);
        transactionId = getIntent().getIntExtra(EXTRA_TRANSACTION_ID, 0);
        salesDate = (Date) getIntent().getSerializableExtra(EXTRA_DATE);
        Log.d(TAG, "onCreate: " + transactionId);
        cartViewModel.getCurrentCart(transactionId).observe(this, carts -> cartList = carts);


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        DividerItemDecoration decoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        rvProductCart.addItemDecoration(decoration);
        productCartRvAdapter = new ProductCartRvAdapter(CartActivity.this);
        rvProductCart.setAdapter(productCartRvAdapter);
        rvProductCart.setLayoutManager(layoutManager);
        productViewModel.getAllProducts().observe(CartActivity.this, products -> {
            productCartRvAdapter.setProducts(products);
            if (productCartRvAdapter.getItemCount() == 0) {
                emptyStateContainer.setVisibility(View.VISIBLE);
                fabBack.setVisibility(View.VISIBLE);
            } else {
                emptyStateContainer.setVisibility(View.GONE);
                fabBack.setVisibility(View.GONE);
            }
        });
//            saveCarts(carts);
        productCartRvAdapter.setOnItemClickListener((v, position) -> {
            Product product = productCartRvAdapter.getProductAtPosition(position);
//                Log.d(TAG, "onItemClick: " + quantity);
            checkProductInCart(product);
            Intent intent = new Intent(CartActivity.this, SalesInputActivity.class);
            intent.putExtra(EXTRA_TRANSACTION_ID, transactionId);
            intent.putExtra(EXTRA_DATE, salesDate);
            startActivity(intent);
            finish();
        });

    }


    public void checkProductInCart(Product productInCart) {
        int productId = productInCart.getId();
        int quantityInCart;
        String productName = productInCart.getName();
        int productPrice = productInCart.getPrice();
        int productStock = productInCart.getStock();
        int idCartdb;
        insertProduct(productId, productName, 1, productStock, productPrice);

    }


    private void UpdateProduct(int id, int productId, String productName, int quantity, int productPrice) {
        Date dateTrans = Calendar.getInstance().getTime();
        int updateQuantity = quantity + 1;
        int subTotal = quantity * productPrice;
        Cart cartUpdate = new Cart(id, transactionId, updateQuantity, productPrice, subTotal, productId, productName, "", dateTrans);
        cartViewModel.insertCarts(cartUpdate);
    }


    private void insertProduct(int productId, String productName, int quantity, int productStock, int productPrice) {
        Date dateTrans = Calendar.getInstance().getTime();
        int subTotal = quantity * productPrice;
//        Log.d(TAG, "insertProduct: transactionId" + transactionId);
        Cart cartInsert = new Cart(transactionId, quantity, productStock, productPrice, subTotal, productId, productName, "", dateTrans);
        cartViewModel.insertCarts(cartInsert);

    }

    @OnClick(R.id.fab_back)
    public void onViewClicked() {
        finish();
    }


//    @OnClick(R.id.fab_add_sales)
//    public void onViewClicked() {
//        Date c = Calendar.getInstance().getTime();
//
//        Cart ca = new Cart(transactionId, 40, 40000, 1600000, 2, "buku", "catatan1", c);
//        cartViewModel.insertCarts(ca);
//        Cart ca2 = new Cart(2, 40, 40000, 1600000, 2, "pulpen", "catatan1", c);
//        cartViewModel.insertCarts(ca2);
//    }


//    @Override
//    public void onChevronClick(View v, int position) {
//            Product productInCart = productCartRvAdapter.getProductAtPosition(position);
//            checkProductInCart(productInCart);
//
//        }


//    @Override
//    public void onItemClick(View v, int position) {
//        Product product = productCartRvAdapter.getProductAtPosition(position);
//        checkProductInCart(product);
//    }
}
