/*
 * Copyright (C) 2018 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package id.qibee.edu.akuntansiukm.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.qibee.edu.akuntansiukm.R;
import id.qibee.edu.akuntansiukm.db.Cart;
import id.qibee.edu.akuntansiukm.util.Helper;

/**
 * Adapter for the RecyclerView that displays a list of words.
 */

public class CartRvAdapter extends RecyclerView.Adapter<CartRvAdapter.WordViewHolder> {

    private static final String TAG = "ProductAdapter";
    private final LayoutInflater mInflater;


    private List<Cart> cartList; // Cached copy of words
    private static ClickListener clickListener;
Helper helper;
    CartRvAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public WordViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_cart, parent, false);
        return new WordViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull WordViewHolder holder, int position) {
        if (cartList != null) {
            Cart current = cartList.get(position);
            Log.d(TAG, "onBind ProductId: "+current.getPoductId());
            Log.d(TAG, "onBind TransactionId: "+current.getTransaction_id());
            Log.d(TAG, "onBind Quantity: "+current.getQuantity());
            holder.textProductName.setText(current.getPoductName());
            holder.textProductPrice.setText(helper.formatRupiah(current.getPrice()));
            holder.textSubTotal.setText(helper.formatRupiah(current.getSubTotal()));
            holder.textProductQuantity.setText(String.valueOf(current.getQuantity()));
        } else {
            // Covers the case of data not being ready yet.
            holder.textProductName.setText("-- Belum ada data --");
        }
    }

    /**
     * Associates a list of words with this adapter
     */
    void setCartList(List<Cart> carts) {
        cartList = carts;
        notifyDataSetChanged();
    }

    /**
     * getItemCount() is called many times, and when it is first called,
     * cartList has not been updated (means initially, it's null, and we can't return null).
     */
    @Override
    public int getItemCount() {
        if (cartList != null)
            return cartList.size();
        else return 0;
    }

    /**
     * Gets the word at a given position.
     * This method is useful for identifying which word
     * was clicked or swiped in methods that handle user events.
     *
     * @param position The position of the word in the RecyclerView
     * @return The word at the given position
     */
    public Cart getCartAtPosition(int position) {
        return cartList.get(position);
    }

    class WordViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_product_name)
        TextView textProductName;
        @BindView(R.id.text_product_price)
        TextView textProductPrice;
        @BindView(R.id.text_product_quantity)
        TextView textProductQuantity;
        @BindView(R.id.text_sub_total)
        TextView textSubTotal;
        @BindView(R.id.text_product_unit)
        TextView textProductUnit;

        private WordViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(view -> clickListener.onItemClick(view, getAdapterPosition()));
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        CartRvAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(View v, int position);
    }

}
