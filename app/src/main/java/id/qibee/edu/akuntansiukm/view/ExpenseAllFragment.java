package id.qibee.edu.akuntansiukm.view;


import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import id.qibee.edu.akuntansiukm.R;
import id.qibee.edu.akuntansiukm.db.Expense;
import id.qibee.edu.akuntansiukm.viewmodel.ExpenseViewModel;
import id.qibee.edu.akuntansiukm.viewmodel.TransactionViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExpenseAllFragment extends Fragment {

    public static final String EXTRA_ID_EXPENSE = "id_cost";
    public static final String EXTRA_TRANS_CODE = "code_cost";
    public static final String EXTRA_COST_NAME = "name_cost";
    public static final String EXTRA_VALUE_COST = "value_cost";
    public static final String EXTRA_NOTE = "note_cost";
    public static final String EXTRA_DATE_EXPENSE = "date_cost";
    @BindView(R.id.rv_month)
    RecyclerView rvMonth;
    @BindView(R.id.rv_expense)
    RecyclerView rvExpense;
    @BindView(R.id.fab_add_expense)
    FloatingActionButton fabAddExpense;
    Unbinder unbinder;
    @BindView(R.id.empty_state_container)
    LinearLayout emptyStateContainer;

    private ExpenseViewModel expenseViewModel;
    private TransactionViewModel transactionViewModel;
    private ExpenseRvAdapter expenseRvAdapter;

    public ExpenseAllFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_expense_all, container, false);
        unbinder = ButterKnife.bind(this, view);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        DividerItemDecoration decoration = new DividerItemDecoration(view.getContext(), DividerItemDecoration.VERTICAL);
        rvExpense.addItemDecoration(decoration);
        expenseRvAdapter = new ExpenseRvAdapter(getContext());
        rvExpense.setAdapter(expenseRvAdapter);
        rvExpense.setLayoutManager(layoutManager);
        expenseViewModel = ViewModelProviders.of(this).get(ExpenseViewModel.class);
        transactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel.class);
        expenseViewModel.getAllExpenses().observe(this, expenses -> {
            expenseRvAdapter.setExpenses(expenses);
            if (expenseRvAdapter.getItemCount() == 0) {
                emptyStateContainer.setVisibility(View.VISIBLE);
            } else {
                emptyStateContainer.setVisibility(View.GONE);
            }
        });

        // RecyclerView to delete the swiped item.
        ItemTouchHelper helper = new ItemTouchHelper(
                new ItemTouchHelper.SimpleCallback(0,
                        ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                    @Override
                    // We are not implementing onMove() in this app.
                    public boolean onMove(RecyclerView recyclerView,
                                          RecyclerView.ViewHolder viewHolder,
                                          RecyclerView.ViewHolder target) {
                        return false;
                    }

                    @Override
                    // When the use swipes a word,
                    // delete that word from the database.
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                        int position = viewHolder.getAdapterPosition();
                        Expense expensesAtPosition = expenseRvAdapter.getExpensesAtPosition(position);
                        Toast.makeText(getContext(),
                                getString(R.string.delete_word_preamble) + " " +
                                        expensesAtPosition.getCostName(), Toast.LENGTH_LONG).show();

                        // Delete the word.
                        expenseViewModel.deleteExpenses(expensesAtPosition);
                        String transactionCode = expensesAtPosition.getTransactionCode();
                        transactionViewModel.deleteByCode(transactionCode);
                    }
                });
        // Attach the item touch helper to the recycler view.
        helper.attachToRecyclerView(rvExpense);

        expenseRvAdapter.setOnItemClickListener((v, position) -> {
            Expense expense = expenseRvAdapter.getExpensesAtPosition(position);
            updateExpense(expense);
        });
        return view;
    }

    private void updateExpense(Expense expense) {
        Intent intent = new Intent(getContext(), ExpenseUpdateActivity.class);
        intent.putExtra(EXTRA_ID_EXPENSE, expense.getId());
        intent.putExtra(EXTRA_TRANS_CODE, expense.getTransactionCode());
        intent.putExtra(EXTRA_COST_NAME, expense.getCostName());
        intent.putExtra(EXTRA_VALUE_COST, expense.getCost());
        intent.putExtra(EXTRA_DATE_EXPENSE, expense.getExpenseDate());
        intent.putExtra(EXTRA_NOTE, expense.getNotes());
        startActivity(intent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.fab_add_expense)
    public void onViewClicked() {
        startActivity(new Intent(getContext(), ExpenseInputActivity.class));

    }
}
