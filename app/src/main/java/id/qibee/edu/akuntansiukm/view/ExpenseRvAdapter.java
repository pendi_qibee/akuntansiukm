/*
 * Copyright (C) 2018 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package id.qibee.edu.akuntansiukm.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.qibee.edu.akuntansiukm.R;
import id.qibee.edu.akuntansiukm.db.Expense;
import id.qibee.edu.akuntansiukm.util.Helper;

/**
 * Adapter for the RecyclerView that displays a list of words.
 */

public class ExpenseRvAdapter extends RecyclerView.Adapter<ExpenseRvAdapter.ExpensesViewHolder> {

    private static final String TAG = "ExpenseAdapter";
    private final LayoutInflater mInflater;



    private List<Expense> expenseList; // Cached copy of words
    private static ClickListener clickListener;

    ExpenseRvAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ExpensesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_expense, parent, false);
        return new ExpensesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ExpensesViewHolder holder, int position) {
        if (expenseList != null) {
            Expense current = expenseList.get(position);
            Log.d(TAG, "onBindViewHolder: " + current.getId());
            holder.textCostName.setText(current.getCostName());
//            SimpleDateFormat sdf = new SimpleDateFormat("MMMM d, yyyy 'at' h:mm a");
//            String date = sdf.format(current.getExpenseDate());
            holder.textCostDate.setText(Helper.getStringFromDate(current.getExpenseDate()));
            holder.textCost.setText(Helper.formatRupiah(current.getCost()));

        } else {
            // Covers the case of data not being ready yet.
            holder.textCostName.setText("Belum Ada Data");

        }
    }

    /**
     * Associates a list of words with this adapter
     */
    void setExpenses(List<Expense> Expense) {
        expenseList = Expense;
        notifyDataSetChanged();
    }

    /**
     * getItemCount() is called many times, and when it is first called,
     * expenseList has not been updated (means initially, it's null, and we can't return null).
     */
    @Override
    public int getItemCount() {
        if (expenseList != null)
            return expenseList.size();
        else return 0;
    }


    public Expense getExpensesAtPosition(int position) {
        return expenseList.get(position);
    }

    class ExpensesViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_cost_name)
        TextView textCostName;
        @BindView(R.id.text_cost_date)
        TextView textCostDate;
        @BindView(R.id.text_cost)
        TextView textCost;
        @BindView(R.id.card_task)
        LinearLayout cardTask;
        private ExpensesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(view -> clickListener.onItemClick(view, getAdapterPosition()));
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        ExpenseRvAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(View v, int position);
    }

}
