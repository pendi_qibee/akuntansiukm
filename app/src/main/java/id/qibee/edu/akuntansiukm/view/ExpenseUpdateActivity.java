package id.qibee.edu.akuntansiukm.view;

import android.app.DatePickerDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;

import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.qibee.edu.akuntansiukm.R;
import id.qibee.edu.akuntansiukm.db.Expense;
import id.qibee.edu.akuntansiukm.util.Helper;
import id.qibee.edu.akuntansiukm.viewmodel.ExpenseViewModel;
import id.qibee.edu.akuntansiukm.viewmodel.TransactionViewModel;

import static id.qibee.edu.akuntansiukm.view.ExpenseAllFragment.EXTRA_COST_NAME;
import static id.qibee.edu.akuntansiukm.view.ExpenseAllFragment.EXTRA_DATE_EXPENSE;
import static id.qibee.edu.akuntansiukm.view.ExpenseAllFragment.EXTRA_ID_EXPENSE;
import static id.qibee.edu.akuntansiukm.view.ExpenseAllFragment.EXTRA_NOTE;
import static id.qibee.edu.akuntansiukm.view.ExpenseAllFragment.EXTRA_TRANS_CODE;
import static id.qibee.edu.akuntansiukm.view.ExpenseAllFragment.EXTRA_VALUE_COST;

public class ExpenseUpdateActivity extends AppCompatActivity {

    @BindView(R.id.input_date)
    TextInputEditText inputDate;
    @BindView(R.id.input_name)
    TextInputEditText inputName;
    @BindView(R.id.input_cost)
    TextInputEditText inputCost;
    @BindView(R.id.input_notes)
    TextInputEditText inputNotes;
    @BindView(R.id.btn_send)
    AppCompatButton btnSend;
    @BindView(R.id.btn_cancel)
    AppCompatButton btnCancel;
    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private String transactionCode, costName, notes, costString;
    private int cost = 0, idCost;
    private Date expenseDate;
    private ExpenseViewModel expenseViewModel;
    private TransactionViewModel transactionViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expense_update);
        ButterKnife.bind(this);
        expenseViewModel = ViewModelProviders.of(this).get(ExpenseViewModel.class);
        transactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel.class);

        idCost = getIntent().getIntExtra(EXTRA_ID_EXPENSE, 0);
        transactionCode = getIntent().getStringExtra(EXTRA_TRANS_CODE);
        costName = getIntent().getStringExtra(EXTRA_COST_NAME);
        cost = getIntent().getIntExtra(EXTRA_VALUE_COST, 0);
        notes = getIntent().getStringExtra(EXTRA_NOTE);
        expenseDate = (Date) getIntent().getSerializableExtra(EXTRA_DATE_EXPENSE);

        setInputContent();
    }

    private void setInputContent() {

        if (expenseDate != null) {
            inputDate.setText(Helper.getStringFromDate(expenseDate));
        }
        inputName.setText(costName);
        inputCost.setText(String.valueOf(cost));
        inputNotes.setText(notes);

    }

    @OnClick({R.id.input_date, R.id.btn_send, R.id.btn_cancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.input_date:
                setDate();
                break;
            case R.id.btn_send:
                saveExpense();
                break;
            case R.id.btn_cancel:
                break;
        }
    }

    private boolean checkName() {
        inputName.setError(null);
        if (inputName.length() == 0) {
            inputName.setError(getString(R.string.input_empty));
            return false;
        }
        return true;
    }


    private boolean checkCost() {
        inputCost.setError(null);
        if (costString.length() == 0) {
            inputCost.setError(getString(R.string.input_empty));
            return false;
        }
        return true;
    }


    private void getTextInput() {
        costName = inputName.getText().toString();
        costString = inputCost.getText().toString();
        notes = inputNotes.getText().toString();
    }

    private void saveExpense() {
        getTextInput();
        if (!checkName() || !checkCost()) {
            Snackbar.make(coordinatorLayout, R.string.input_not_complete,
                    Snackbar.LENGTH_SHORT)
                    .show();
        } else {
            cost = Integer.parseInt(costString);
            Expense expense = new Expense(idCost, transactionCode, cost, costName, notes, expenseDate);
            expenseViewModel.updateExpenses(expense);
//            Transaction transaction = new Transaction(id, transactionCode)
            finish();
        }
    }

    private void setDate() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                (view, year, monthOfYear, dayOfMonth) -> {
                    inputDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    expenseDate = Helper.getDateFromDatePicker(year, monthOfYear, dayOfMonth);
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }
}
