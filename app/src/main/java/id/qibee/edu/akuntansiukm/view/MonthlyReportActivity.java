package id.qibee.edu.akuntansiukm.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.qibee.edu.akuntansiukm.BasePdfActivity;
import id.qibee.edu.akuntansiukm.MonthlyReportPdfActivity;
import id.qibee.edu.akuntansiukm.R;

public class MonthlyReportActivity extends BasePdfActivity implements MounthlyReportFragment.OnClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.main_container)
    FrameLayout mainContainer;
    FragmentManager manager = getSupportFragmentManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monthly_report);
        ButterKnife.bind(this);

        manager.beginTransaction()
                .replace(R.id.main_container, new MounthlyReportFragment())
                .commit();

    }

    @Override
    public void onFabClick(String monthDate,String monthWord) {
        showPdfDone();

        Intent intent = new Intent(this, MonthlyReportPdfActivity.class);
        intent.putExtra(MONTH_EXTRA, monthDate);
        intent.putExtra(MONTHWORD_EXTRA, monthWord);
        startActivity(intent);

    }

    private void showPdfDone() {
        new AlertDialog.Builder(MonthlyReportActivity.this, R.style.Theme_AppCompat_DayNight_Dialog_Alert)
                .setMessage("Laporan format PDF selesai dibuat.\n" +
                        "Silakan buka di folder Document")
                .setCancelable(false)
                .setPositiveButton("OK", (dialog, id) -> MonthlyReportActivity.super.onBackPressed())
//                .setNegativeButton("No", null)
                .show();
    }

}
