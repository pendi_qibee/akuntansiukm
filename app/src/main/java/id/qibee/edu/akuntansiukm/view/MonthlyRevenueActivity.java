package id.qibee.edu.akuntansiukm.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.qibee.edu.akuntansiukm.BasePdfActivity;
import id.qibee.edu.akuntansiukm.R;
import id.qibee.edu.akuntansiukm.RevenueMounthlyPdfActivity;

public class MonthlyRevenueActivity extends BasePdfActivity implements  MonthlyRevenueFragment.OnClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.main_container)
    FrameLayout mainContainer;
    FragmentManager manager = getSupportFragmentManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_revenue_monthly);
        ButterKnife.bind(this);

        manager.beginTransaction()
                .replace(R.id.main_container, new MonthlyRevenueFragment())
                .commit();
    }

    @Override
    public void onFabClick(String monthDate, int salesMonth, int purchaseMonth, int expenseMonth, int revenueMonth) {
        showPdfDone();


        Intent intent = new Intent(this, RevenueMounthlyPdfActivity.class);
        intent.putExtra(MONTH_EXTRA, monthDate);
        intent.putExtra(SALES_EXTRA, salesMonth);
        intent.putExtra(PURCHASE_EXTRA, purchaseMonth);
        intent.putExtra(EXPENSE_EXTRA, expenseMonth);
        intent.putExtra(REVENUE_EXTRA, revenueMonth);
        startActivity(intent);
    }

    private void showPdfDone() {
        new AlertDialog.Builder(MonthlyRevenueActivity.this, R.style.Theme_AppCompat_DayNight_Dialog_Alert)
                .setMessage("Laporan format PDF selesai dibuat.\n" +
                        "Silakan buka di folder Document")
                .setCancelable(false)
                .setPositiveButton("OK", (dialog, id) -> MonthlyRevenueActivity.super.onBackPressed())
//                .setNegativeButton("No", null)
                .show();
    }

}
