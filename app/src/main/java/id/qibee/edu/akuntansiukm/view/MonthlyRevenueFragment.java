package id.qibee.edu.akuntansiukm.view;


import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import id.qibee.edu.akuntansiukm.QibeeApp;
import id.qibee.edu.akuntansiukm.R;
import id.qibee.edu.akuntansiukm.db.Month;
import id.qibee.edu.akuntansiukm.util.Helper;
import id.qibee.edu.akuntansiukm.viewmodel.ExpenseViewModel;
import id.qibee.edu.akuntansiukm.viewmodel.PurchasesViewModel;
import id.qibee.edu.akuntansiukm.viewmodel.SalesViewModel;
import id.qibee.edu.akuntansiukm.viewmodel.TransactionViewModel;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class MonthlyRevenueFragment extends Fragment {


    @BindView(R.id.rv_month)
    RecyclerView rvMonth;
    @BindView(R.id.text_periode)
    TextView textPeriode;
    @BindView(R.id.header_container)
    LinearLayout headerContainer;
    @BindView(R.id.text_total_sales)
    TextView textTotalSales;
    @BindView(R.id.text_total_purchase)
    TextView textTotalPurchase;
    @BindView(R.id.text_total_cost)
    TextView textTotalCost;
    @BindView(R.id.text_total_revenue)
    TextView textTotalRevenue;
    @BindView(R.id.layout_mounthly)
    LinearLayout layoutMounthly;
    @BindView(R.id.empty_state_container)
    LinearLayout emptyStateContainer;
    Unbinder unbinder;
    @BindView(R.id.text_company_name)
    TextView textCompanyName;
    @BindView(R.id.button_print_pdf)
    MaterialRippleLayout buttonPrintPdf;



    private MonthRvAdapter monthRvAdapter;

    private SalesViewModel salesViewModel;
    private PurchasesViewModel purchasesViewModel;
    private ExpenseViewModel expenseViewModel;
    private TransactionViewModel transactionViewModel;
    String monthDate;
    //    private ArrayList<String> monthList = new ArrayList<>();
    private List<Month> monthList = new ArrayList<>();
    private OnClickListener listener;

    @OnClick(R.id.button_print_pdf)
    public void onViewClicked() {
        listener.onFabClick(monthDate,salesMonth,purchaseMonth,expenseMonth,revenueMonth);
    }


    public interface OnClickListener {
        void onFabClick(String monthDate, int salesMonth, int purchaseMonth, int expenseMonth, int revenueMonth);
    }

    public MonthlyRevenueFragment() {
        // Required empty public constructor
    }

    int salesMonth, purchaseMonth, expenseMonth, revenueMonth;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_monthly_revenue, container, false);
        unbinder = ButterKnife.bind(this, view);

        monthRvAdapter = new MonthRvAdapter(getContext());
        RecyclerView.LayoutManager layoutManagerMonth = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, true);

        rvMonth.setAdapter(monthRvAdapter);
        rvMonth.setLayoutManager(layoutManagerMonth);

        salesViewModel = ViewModelProviders.of(this).get(SalesViewModel.class);
        purchasesViewModel = ViewModelProviders.of(this).get(PurchasesViewModel.class);
        expenseViewModel = ViewModelProviders.of(this).get(ExpenseViewModel.class);
        transactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel.class);
        transactionViewModel.getMonth().observe(this, months -> monthRvAdapter.setSales(months));

        String name = QibeeApp.getInstance().getUsername();
        Timber.d("name %s", name);
        if (name == null) {
            textCompanyName.setText("NAMA BADAN USAHA");
        } else {
        textCompanyName.setText(name);
        }

        monthRvAdapter.setOnItemClickListener((v, position) -> {
            Month month = monthRvAdapter.getMonthAtPosition(position);
            if (month != null) {
                monthDate = month.getMonth();
                Timber.d("MonthDate %s", monthDate);
                getAllReport(getMonthString(monthDate));
                textPeriode.setText("BULAN " + monthDate + " 2018");
            }
        });

        if (monthDate == null || monthDate.equalsIgnoreCase("")) {
            getCurrentMonth();
            Timber.d("getCurrentMonth execute");
        }
        return view;
    }

    private void getCurrentMonth() {
        transactionViewModel.getMonth().observe(this, months -> {
            if (months != null && months.size() != 0) {
                monthDate = months.get(0).getMonth();
                Timber.d("getCurrentMonth: %s", monthDate);
                String monthStringlocal = getMonthString(monthDate);
                Timber.d("monthstringLocal = %s", monthStringlocal);
                textPeriode.setText("BULAN " + monthDate + " 2018");
                getAllReport(monthStringlocal);
                layoutMounthly.setVisibility(View.VISIBLE);
                emptyStateContainer.setVisibility(View.GONE);
            } else {
                layoutMounthly.setVisibility(View.GONE);
                buttonPrintPdf.setVisibility(View.GONE);
                emptyStateContainer.setVisibility(View.VISIBLE);
            }
        });

    }

    @SuppressLint("StaticFieldLeak")
    private void getAllReport(String monthString) {
        Timber.d("getAllReport monthstring %s", monthString);

        new AsyncTask<Void, Void, Void>() {
            int totalSalesInMonth = 0;
            int purchaseInMonth = 0;
            int expenseInMonth = 0;
            int totalRevenue = 0;

            @Override
            protected Void doInBackground(Void... voids) {
                totalSalesInMonth = salesViewModel.getTotalMonth(monthString).getTotal();
                Timber.d("totalsales :%s", totalSalesInMonth);
                purchaseInMonth = purchasesViewModel.getTotalMonth(monthString).getTotal();
                Timber.d("totalpurchase :%s", purchaseInMonth);

                expenseInMonth = expenseViewModel.getTotalMonth(monthString).getTotal();
                Timber.d("totalexpense :%s", expenseInMonth);

                totalRevenue = totalSalesInMonth - purchaseInMonth - expenseInMonth;

                salesMonth = totalSalesInMonth;
                purchaseMonth = purchaseInMonth;
                expenseMonth = expenseInMonth;
                revenueMonth = totalRevenue;
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                textTotalSales.setText(Helper.formatRupiah(totalSalesInMonth));
                textTotalPurchase.setText(String.format(getResources().getString(R.string.cash_placeholder), Helper.formatRupiah(purchaseInMonth)));
                textTotalCost.setText(String.format(getResources().getString(R.string.cash_placeholder), Helper.formatRupiah(expenseInMonth)));
                textTotalRevenue.setText(Helper.formatRupiah(totalRevenue));
            }
        }.execute();
    }


    private String getMonthString(String month) {
        String monthString = "";
        switch (month) {
            case "JANUARI":
                monthString = "01";
                break;
            case "FEBRUARI":
                monthString = "02";
                break;
            case "MARET":
                monthString = "03";
                break;
            case "APRIL":
                monthString = "04";
                break;
            case "MEI":
                monthString = "05";
                break;
            case "JUNI":
                monthString = "06";
                break;
            case "JULI":
                monthString = "07";
                break;
            case "AGUSTUS":
                monthString = "08";
                break;
            case "SEPTEMBER":
                monthString = "09";
                break;
            case "OKTOBER":
                monthString = "10";
                break;
            case "NOVEMBER":
                monthString = "11";
                break;
            case "DESEMBER":
                monthString = "12";
                break;
        }

        return monthString;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnClickListener) {
            listener = (OnClickListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement OnItemSelectedListener");
        }
    }
}

