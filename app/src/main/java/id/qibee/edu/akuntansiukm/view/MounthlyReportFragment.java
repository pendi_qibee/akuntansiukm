package id.qibee.edu.akuntansiukm.view;


import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import id.qibee.edu.akuntansiukm.QibeeApp;
import id.qibee.edu.akuntansiukm.R;
import id.qibee.edu.akuntansiukm.db.Expense;
import id.qibee.edu.akuntansiukm.db.Month;
import id.qibee.edu.akuntansiukm.db.Purchase;
import id.qibee.edu.akuntansiukm.db.Sales;
import id.qibee.edu.akuntansiukm.util.Helper;
import id.qibee.edu.akuntansiukm.viewmodel.ExpenseViewModel;
import id.qibee.edu.akuntansiukm.viewmodel.PurchasesViewModel;
import id.qibee.edu.akuntansiukm.viewmodel.SalesViewModel;
import id.qibee.edu.akuntansiukm.viewmodel.TransactionViewModel;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class MounthlyReportFragment extends Fragment {


    @BindView(R.id.rv_month)
    RecyclerView rvMonth;
    @BindView(R.id.rv_sales_mounthly)
    RecyclerView rvSalesMounthly;
    @BindView(R.id.text_total_sales)
    TextView textTotalSales;
    @BindView(R.id.rv_purchase_mounthly)
    RecyclerView rvPurchaseMounthly;
    @BindView(R.id.text_total_purchase)
    TextView textTotalPurchase;
    @BindView(R.id.rv_expense_mounthly)
    RecyclerView rvExpenseMounthly;
    @BindView(R.id.text_total_expense)
    TextView textTotalExpense;
    @BindView(R.id.empty_state_container)
    LinearLayout emptyStateContainer;
    Unbinder unbinder;
    @BindView(R.id.header_container)
    LinearLayout headerContainer;
    @BindView(R.id.layout_mounthly)
    LinearLayout layoutMounthly;
    //    @BindView(R.id.fab_monthly_pdf)
//    FloatingActionButton fabMonthlyPdf;
    @BindView(R.id.text_name_inreport)
    TextView textNameInreport;
    @BindView(R.id.text_monthreport)
    TextView textMonthreport;
    @BindView(R.id.button_print_pdf)
    MaterialRippleLayout buttonPrintPdf;

    private MonthRvAdapter monthRvAdapter;
    private SalesReportRvAdapter salesReportRvAdapter;
    private PurchaseReportRvAdapter purchaseReportRvAdapter;
    private ExpenseReportRvAdapter expenseReportRvAdapter;

    private SalesViewModel salesViewModel;
    private PurchasesViewModel purchasesViewModel;
    private ExpenseViewModel expenseViewModel;
    private TransactionViewModel transactionViewModel;
    String monthDate;
    //    private ArrayList<String> monthList = new ArrayList<>();
    private List<Month> monthList = new ArrayList<>();
    private List<Sales> salesList = new ArrayList<>();
    private List<Purchase> purchaseList = new ArrayList<>();
    private List<Expense> expenseList = new ArrayList<>();
    private int salesInMonth, purchaseInMonth, expenseInMonth, totalSales, totalPurchase, totalExpense;

    private OnClickListener listener;
    private String monthStringlocal;

    @OnClick(R.id.button_print_pdf)
    public void onViewClicked() {
        listener.onFabClick(monthStringlocal, monthDate);
    }

    public interface OnClickListener {
        void onFabClick(String monthDate, String monthStringLocal);
    }

    public MounthlyReportFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mounthly_report, container, false);
        unbinder = ButterKnife.bind(this, view);

        textNameInreport.setText(QibeeApp.getInstance().getUsername());

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        RecyclerView.LayoutManager layoutManagerPurchases = new LinearLayoutManager(view.getContext());
        RecyclerView.LayoutManager layoutManagerExpense = new LinearLayoutManager(view.getContext());
        RecyclerView.LayoutManager layoutManagerMonth = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, true);
        DividerItemDecoration decoration = new DividerItemDecoration(view.getContext(), DividerItemDecoration.VERTICAL);
        DividerItemDecoration decorationForPurchase = new DividerItemDecoration(view.getContext(), DividerItemDecoration.VERTICAL);
        DividerItemDecoration decorationForExpense = new DividerItemDecoration(view.getContext(), DividerItemDecoration.VERTICAL);
        rvSalesMounthly.addItemDecoration(decoration);
        rvPurchaseMounthly.addItemDecoration(decorationForPurchase);
        rvExpenseMounthly.addItemDecoration(decorationForExpense);

        monthRvAdapter = new MonthRvAdapter(getContext());

        salesReportRvAdapter = new SalesReportRvAdapter(getContext());
        purchaseReportRvAdapter = new PurchaseReportRvAdapter(getContext());
        expenseReportRvAdapter = new ExpenseReportRvAdapter(getContext());


        //SetAdapter

        rvMonth.setAdapter(monthRvAdapter);
        rvMonth.setLayoutManager(layoutManagerMonth);

        rvSalesMounthly.setAdapter(salesReportRvAdapter);
        rvSalesMounthly.setLayoutManager(layoutManager);

        rvPurchaseMounthly.setAdapter(purchaseReportRvAdapter);
        rvPurchaseMounthly.setLayoutManager(layoutManagerPurchases);

        rvExpenseMounthly.setAdapter(expenseReportRvAdapter);
        rvExpenseMounthly.setLayoutManager(layoutManagerExpense);

        //ViewModel
        salesViewModel = ViewModelProviders.of(this).get(SalesViewModel.class);
        purchasesViewModel = ViewModelProviders.of(this).get(PurchasesViewModel.class);
        expenseViewModel = ViewModelProviders.of(this).get(ExpenseViewModel.class);
        transactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel.class);

        transactionViewModel.getMonth().observe(this, months -> monthRvAdapter.setSales(months));
        monthRvAdapter.setOnItemClickListener((v, position) -> {
            Month month = monthRvAdapter.getMonthAtPosition(position);
            if (month != null) {
                monthDate = month.getMonth();
                Timber.d("MonthDate %s", monthDate);
                getAllReport(getMonthString(monthDate));
            }
        });

        if (monthDate == null || monthDate.equalsIgnoreCase("")) {
            getCurrentMonth();
            Timber.d("getCurrentMonth execute");
        }

        salesReportRvAdapter.setOnItemClickListener((v, position) -> {
            Sales sales = salesReportRvAdapter.getSalesAtPosition(position);
//            launchDetailSalesActivity(sales);
        });
        return view;
    }


    private void getCurrentMonth() {
        transactionViewModel.getMonth().observe(this, months -> {
            if (months != null && months.size() != 0) {
                monthDate = months.get(0).getMonth();
                Timber.d("getCurrentMonth: %s", monthDate);
                monthStringlocal = getMonthString(monthDate);
                Timber.d("monthstringLocal = %s", monthStringlocal);
                getAllReport(monthStringlocal);
                layoutMounthly.setVisibility(View.VISIBLE);
                emptyStateContainer.setVisibility(View.GONE);
            } else {
                layoutMounthly.setVisibility(View.GONE);
                buttonPrintPdf.setVisibility(View.GONE);
                emptyStateContainer.setVisibility(View.VISIBLE);
            }
        });

    }

    private String getMonthString(String month) {
        textMonthreport.setText(String.format(getString(R.string.month_placeholder), month));
        String monthString = "";
        switch (month) {
            case "JANUARI":
                monthString = "01";
                break;
            case "FEBRUARI":
                monthString = "02";
                break;
            case "MARET":
                monthString = "03";
                break;
            case "APRIL":
                monthString = "04";
                break;
            case "MEI":
                monthString = "05";
                break;
            case "JUNI":
                monthString = "06";
                break;
            case "JULI":
                monthString = "07";
                break;
            case "AGUSTUS":
                monthString = "08";
                break;
            case "SEPTEMBER":
                monthString = "09";
                break;
            case "OKTOBER":
                monthString = "10";
                break;
            case "NOVEMBER":
                monthString = "11";
                break;
            case "DESEMBER":
                monthString = "12";
                break;
        }

        return monthString;
    }

    @SuppressLint("StaticFieldLeak")
    private void getAllReport(String monthString) {
        Timber.d("getAllReport monthstring %s", monthString);
        salesViewModel.getSales(monthString).observe(this, sales -> {
            if (sales != null) {
                salesReportRvAdapter.setSales(sales);

            }

        });
        purchasesViewModel.getPurchasesInMonth(monthString).observe(this, purchases -> {
            if (purchases != null) {
                purchaseReportRvAdapter.setPurchases(purchases);
                purchaseList = purchases;
            }
        });

        expenseViewModel.getExpenseInMonth(monthString).observe(this, expenses -> {
            if (expenses != null) {
                expenseReportRvAdapter.setExpenses(expenses);
                expenseList = expenses;
            }

        });
        new AsyncTask<Void, Void, Void>() {
            int totalSalesInMonth = 0;
            int purchaseInMonth = 0;
            int expenseInMonth = 0;

            @Override
            protected Void doInBackground(Void... voids) {
                totalSalesInMonth = salesViewModel.getTotalMonth(monthString).getTotal();
                Timber.d("totalsales :%s", totalSalesInMonth);
                purchaseInMonth = purchasesViewModel.getTotalMonth(monthString).getTotal();
                Timber.d("totalpurchase :%s", purchaseInMonth);

                expenseInMonth = expenseViewModel.getTotalMonth(monthString).getTotal();
                Timber.d("totalexpense :%s", expenseInMonth);

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                textTotalSales.setText(Helper.formatRupiah(totalSalesInMonth));
                textTotalPurchase.setText(Helper.formatRupiah(purchaseInMonth));
                textTotalExpense.setText(Helper.formatRupiah(expenseInMonth));

                totalSales = totalSalesInMonth;
                totalPurchase = purchaseInMonth;
                totalExpense = expenseInMonth;
            }
        }.execute();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnClickListener) {
            listener = (OnClickListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement OnItemSelectedListener");
        }
    }
}
