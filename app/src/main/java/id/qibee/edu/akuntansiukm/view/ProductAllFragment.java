package id.qibee.edu.akuntansiukm.view;


import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import id.qibee.edu.akuntansiukm.R;
import id.qibee.edu.akuntansiukm.db.Product;
import id.qibee.edu.akuntansiukm.viewmodel.ProductViewModel;

import static id.qibee.edu.akuntansiukm.view.ProductUpdateActivity.EXTRA_ID_PRODUCT;
import static id.qibee.edu.akuntansiukm.view.ProductUpdateActivity.EXTRA_NAME_PRODUCT;
import static id.qibee.edu.akuntansiukm.view.ProductUpdateActivity.EXTRA_PRICE_PRODUCT;
import static id.qibee.edu.akuntansiukm.view.ProductUpdateActivity.EXTRA_STOCK_PRODUCT;
import static id.qibee.edu.akuntansiukm.view.ProductUpdateActivity.EXTRA_UNIT_PRODUCT;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductAllFragment extends Fragment implements View.OnClickListener {

    @BindView(R.id.rv_product)
    RecyclerView rvProduct;
    @BindView(R.id.fab_add_report)
    FloatingActionButton fabAddReport;
    Unbinder unbinder;
    @BindView(R.id.empty_state_container)
    LinearLayout emptyStateContainer;
    private ProductViewModel productViewModel;

    private ProductRvAdapter productRvAdapter;

    public ProductAllFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        layoutBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_product_all, container, false);
        View view = inflater.inflate(R.layout.fragment_product_all, container, false);
        unbinder = ButterKnife.bind(this, view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        DividerItemDecoration decoration = new DividerItemDecoration(view.getContext(), DividerItemDecoration.VERTICAL);
        rvProduct.addItemDecoration(decoration);
        productRvAdapter = new ProductRvAdapter(getContext());
        rvProduct.setAdapter(productRvAdapter);
        rvProduct.setLayoutManager(layoutManager);
        fabAddReport.setOnClickListener(this);
        productViewModel = ViewModelProviders.of(this).get(ProductViewModel.class);
        productViewModel.getAllProducts().observe(this, products -> {
            productRvAdapter.setProducts(products);
            if (productRvAdapter.getItemCount() == 0) {
                emptyStateContainer.setVisibility(View.VISIBLE);
            } else {
                emptyStateContainer.setVisibility(View.GONE);
            }
        });

        // Add the functionality to swipe items in the
        // RecyclerView to delete the swiped item.
        ItemTouchHelper helper = new ItemTouchHelper(
                new ItemTouchHelper.SimpleCallback(0,
                        ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                    @Override
                    // We are not implementing onMove() in this app.
                    public boolean onMove(RecyclerView recyclerView,
                                          RecyclerView.ViewHolder viewHolder,
                                          RecyclerView.ViewHolder target) {
                        return false;
                    }

                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                        int position = viewHolder.getAdapterPosition();
                        Product product = productRvAdapter.getProductAtPosition(position);
                        Toast.makeText(getContext(),
                                getString(R.string.delete_word_preamble) + " " +
                                        product.getName(), Toast.LENGTH_LONG).show();

                        // Delete the word.
                        productViewModel.deleteProduct(product);
                    }
                });
        // Attach the item touch helper to the recycler view.
        helper.attachToRecyclerView(rvProduct);

        productRvAdapter.setOnItemClickListener((v, position) -> {
            Product product = productRvAdapter.getProductAtPosition(position);
            ProductAllFragment.this.launchUpdateActivity(product);
        });

//        return layoutBinding.getRoot();



        return view;

    }

    private void launchUpdateActivity(Product product) {
        Intent intent = new Intent(getContext(), ProductUpdateActivity.class);
        intent.putExtra(EXTRA_ID_PRODUCT, product.getId());
        intent.putExtra(EXTRA_NAME_PRODUCT, product.getName());
        intent.putExtra(EXTRA_PRICE_PRODUCT, product.getPrice());
        intent.putExtra(EXTRA_UNIT_PRODUCT, product.getUnit());
        intent.putExtra(EXTRA_STOCK_PRODUCT, product.getStock());
        startActivity(intent);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_add_report:
                startActivity(new Intent(getContext(), ProductInputActivity.class));
                break;

        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
