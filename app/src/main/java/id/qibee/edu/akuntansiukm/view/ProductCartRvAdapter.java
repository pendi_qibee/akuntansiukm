/*
 * Copyright (C) 2018 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package id.qibee.edu.akuntansiukm.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.qibee.edu.akuntansiukm.R;
import id.qibee.edu.akuntansiukm.db.Product;
import id.qibee.edu.akuntansiukm.util.Helper;

/**
 * Adapter for the RecyclerView that displays a list of words.
 */

public class ProductCartRvAdapter extends RecyclerView.Adapter<ProductCartRvAdapter.ProductCartViewHolder> {

    private static final String TAG = "ProductAdapter";
    private final LayoutInflater mInflater;
    private List<Product> mProduct;
    private static ClickListener clickListener;
//    private static QuantityListener quantityListener;

    private View.OnClickListener increaseClickListener;
    private View.OnClickListener decreaseClickListener;
    private View.OnClickListener viewClickListener;
    Context context;

    ProductCartRvAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ProductCartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_product_cart, parent, false);
        return new ProductCartViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductCartViewHolder holder, int position) {
        if (mProduct != null) {
            Product current = mProduct.get(position);
            Log.d(TAG, "onBindViewHolder: " + current.getName());
            holder.textTaskTitle.setText(current.getName());
            holder.textPrice.setText(Helper.formatRupiah(current.getPrice()));
            holder.textUnit.setText(String.valueOf(current.getUnit()));
            holder.textStock.setText(String.valueOf(current.getStock()));
            //experiment
//            holder.quantity.setText(String.valueOf(current.getStock()));

//            holder.imgIncrease.setOnClickListener(increaseClickListener);
//            holder.imgDecrease.setOnClickListener(decreaseClickListener);
//holder.imgIncrease.setOnClickListener(view -> {
//                holder.layoutNotes.setVisibility(View.VISIBLE);
//                holder.viewLine.setVisibility(View.VISIBLE);
//                quantityListener.onChevronClick(view, position);
//            });


        } else {
            // Covers the case of data not being ready yet.
            holder.textTaskTitle.setText("Belum ada data");
        }
    }

    /**
     * Associates a list of words with this adapter
     */
    void setProducts(List<Product> products) {
        mProduct = products;
        notifyDataSetChanged();
    }

    /**
     * getItemCount() is called many times, and when it is first called,
     * mProduct has not been updated (means initially, it's null, and we can't return null).
     */
    @Override
    public int getItemCount() {
        if (mProduct != null)
            return mProduct.size();
        else return 0;
    }

    /**
     * Gets the word at a given position.
     * This method is useful for identifying which word
     * was clicked or swiped in methods that handle user events.
     *
     * @param position The position of the word in the RecyclerView
     * @return The word at the given position
     */
    public Product getProductAtPosition(int position) {
        return mProduct.get(position);
    }

    @OnClick(R.id.button_delete_notes)
    public void onViewClicked() {
    }

    class ProductCartViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_task_title)
        TextView textTaskTitle;
//        @BindView(R.id.img_decrease)
//        ImageView imgDecrease;
//        @BindView(R.id.quantity)
//        TextView quantity;
//        @BindView(R.id.img_increase)
//        ImageView imgIncrease;
        @BindView(R.id.text_price)
        TextView textPrice;
        @BindView(R.id.text_unit)
        TextView textUnit;
        @BindView(R.id.text_stock)
        TextView textStock;
        @BindView(R.id.input_notes)
        EditText inputNotes;
        @BindView(R.id.button_delete_notes)
        ImageView buttonDeleteNotes;
        @BindView(R.id.layout_notes)
        LinearLayout layoutNotes;
        @BindView(R.id.card_task)
        LinearLayout cardTask;
        @BindView(R.id.view_line)
        View viewLine;

        private ProductCartViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(view -> clickListener.onItemClick(view, getAdapterPosition()));
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    switch (view.getId()) {
//                        case R.id.img_increase:
//                            quantityListener.onChevronClick(view, ProductCartViewHolder.this.getAdapterPosition());
                            clickListener.onItemClick(view, ProductCartViewHolder.this.getAdapterPosition());
//                    }
                }
            });
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        ProductCartRvAdapter.clickListener = clickListener;
    }
//
//    public void setOnQuantityClickListener(QuantityListener quantityClickListener) {
//        ProductCartRvAdapter.quantityListener = quantityClickListener;
//    }
//
    public interface ClickListener {
        void onItemClick(View v, int position);
    }
//
//    public interface QuantityListener extends View.OnClickListener {
//        void onChevronClick(View v, int position);

//        @Override
//        void onClick(View view);
//
//    }

    public View.OnClickListener getIncreaseClickListener() {
        return increaseClickListener;
    }
}
