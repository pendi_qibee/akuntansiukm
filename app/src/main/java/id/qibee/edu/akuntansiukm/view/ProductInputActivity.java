package id.qibee.edu.akuntansiukm.view;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.qibee.edu.akuntansiukm.R;
import id.qibee.edu.akuntansiukm.db.Product;
import id.qibee.edu.akuntansiukm.viewmodel.ProductViewModel;

public class ProductInputActivity extends AppCompatActivity {
    //ActivityProductInputBinding layoutBinding;
    String name, unit, priceString, stockString;
    int stock, price;
    @BindView(R.id.input_name)
    TextInputEditText inputName;
    @BindView(R.id.input_unit)
    TextInputEditText inputUnit;
    @BindView(R.id.input_price)
    TextInputEditText inputPrice;
    @BindView(R.id.input_stock)
    TextInputEditText inputStock;
    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.btn_send)
    AppCompatButton btnSend;
    @BindView(R.id.btn_cancel)
    AppCompatButton btnCancel;
    //    ActivityProductInputBinding layoutBinding;
    private ProductViewModel productViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        layoutBinding = DataBindingUtil.setContentView(this, R.layout.activity_product_input);
        setContentView(R.layout.activity_product_input);
        ButterKnife.bind(this);
        productViewModel = ViewModelProviders.of(this).get(ProductViewModel.class);
    }

    private boolean checkName() {
        inputName.setError(null);
        if (name.length() == 0) {
            inputName.setError(getString(R.string.input_empty));
            return false;
        }
        return true;
    }

    private boolean checkUnit() {
        inputUnit.setError(null);
        if (unit.length() == 0) {
            inputUnit.setError(getString(R.string.input_empty));
            return false;
        }
        return true;
    }

    private boolean checkPrice() {
        inputPrice.setError(null);
        if (priceString.length() == 0) {
            inputPrice.setError(getString(R.string.input_empty));
            return false;
        }
        return true;
    }

    private boolean checkStock() {
        inputStock.setError(null);
        if (stockString.length() == 0) {
            inputStock.setError(getString(R.string.input_empty));
            return false;
        }
        return true;
    }


    private void getTextInput() {
        name = inputName.getText().toString();
        unit = inputUnit.getText().toString();
        stockString = inputStock.getText().toString();
        priceString = inputPrice.getText().toString();

    }

    @OnClick({R.id.btn_send, R.id.btn_cancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_send:
                addProduct();
                break;
            case R.id.btn_cancel:
                finish();
                break;
        }
    }

    private void addProduct() {
        getTextInput();
        if (!checkName() || !checkUnit() || !checkPrice() || !checkStock()) {
            Snackbar.make(coordinatorLayout, R.string.input_not_complete,
                    Snackbar.LENGTH_SHORT)
                    .show();
        } else {
            price = Integer.parseInt(inputPrice.getText().toString());
            stock = Integer.parseInt(inputStock.getText().toString());
            Product product = new Product(stock, name, unit, price);
            productViewModel.insertProduct(product);
            finish();
        }
    }
}
