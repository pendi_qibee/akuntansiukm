/*
 * Copyright (C) 2018 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package id.qibee.edu.akuntansiukm.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import id.qibee.edu.akuntansiukm.R;
import id.qibee.edu.akuntansiukm.db.Product;
import id.qibee.edu.akuntansiukm.util.Helper;
import timber.log.Timber;

/**
 * Adapter for the RecyclerView that displays a list of words.
 */

public class ProductRvAdapter extends RecyclerView.Adapter<ProductRvAdapter.WordViewHolder> {

    private final LayoutInflater mInflater;
    private List<Product> mProduct; // Cached copy of words
	private static ClickListener clickListener;

    ProductRvAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public WordViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_product, parent, false);
        return new WordViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull WordViewHolder holder, int position) {
        if (mProduct != null) {
            Product current = mProduct.get(position);
            Timber.d("onBindViewHolder: %s", current.getName());
            holder.productNameItemView.setText(current.getName());
            holder.priceItemView.setText("Harga : "+ Helper.formatRupiah(current.getPrice()));
            holder.unitItemView.setText(String.valueOf(current.getUnit()));
            holder.stockItemView.setText("Stock : "+String.valueOf(current.getStock()));
        } else {
            // Covers the case of data not being ready yet.
            holder.productNameItemView.setText("Belum ada data");
        }
    }

    /**
     * Associates a list of words with this adapter
    */
    void setProducts(List<Product> products) {
        mProduct = products;
        notifyDataSetChanged();
    }

    /**
     * getItemCount() is called many times, and when it is first called,
     * mProduct has not been updated (means initially, it's null, and we can't return null).
     */
    @Override
    public int getItemCount() {
        if (mProduct != null)
            return mProduct.size();
        else return 0;
    }

    /**
     * Gets the word at a given position.
     * This method is useful for identifying which word
     * was clicked or swiped in methods that handle user events.
     *
     * @param position The position of the word in the RecyclerView
     * @return The word at the given position
     */
    public Product getProductAtPosition(int position) {
        return mProduct.get(position);
    }

    class WordViewHolder extends RecyclerView.ViewHolder {
        private final TextView productNameItemView;
        private final TextView priceItemView;
        private final TextView unitItemView;
        private final TextView stockItemView;

        private WordViewHolder(View itemView) {
            super(itemView);
            productNameItemView = itemView.findViewById(R.id.text_task_title);
            priceItemView = itemView.findViewById(R.id.text_price);
            unitItemView = itemView.findViewById(R.id.text_unit);
            stockItemView = itemView.findViewById(R.id.text_stock);
            itemView.setOnClickListener(view -> clickListener.onItemClick(view, WordViewHolder.this.getAdapterPosition()));
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        ProductRvAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(View v, int position);
    }

}
