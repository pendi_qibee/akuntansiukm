package id.qibee.edu.akuntansiukm.view;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.qibee.edu.akuntansiukm.R;
import id.qibee.edu.akuntansiukm.db.Product;
import id.qibee.edu.akuntansiukm.viewmodel.ProductViewModel;

public class ProductUpdateActivity extends AppCompatActivity {
    public static final String EXTRA_ID_PRODUCT = "id_product";
    public static final String EXTRA_NAME_PRODUCT = "name_product";
    public static final String EXTRA_PRICE_PRODUCT = "price_product";
    public static final String EXTRA_UNIT_PRODUCT = "unit_product";
    public static final String EXTRA_STOCK_PRODUCT = "stock_product";
    String name, unit;
    int idProduct,stock, price;
    @BindView(R.id.input_name)
    TextInputEditText inputName;
    @BindView(R.id.input_unit)
    TextInputEditText inputUnit;
    @BindView(R.id.input_price)
    TextInputEditText inputPrice;
    @BindView(R.id.input_stock)
    TextInputEditText inputStock;
    @BindView(R.id.btn_update)
    AppCompatButton btnUpdate;
    @BindView(R.id.btn_cancel)
    AppCompatButton btnCancel;
    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;
    private ProductViewModel productViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//       layoutBinding= DataBindingUtil.setContentView(this, R.layout.activity_product_update);
        setContentView(R.layout.activity_product_update);
        ButterKnife.bind(this);
        productViewModel = ViewModelProviders.of(this).get(ProductViewModel.class);

        name = getIntent().getStringExtra(EXTRA_NAME_PRODUCT);
        unit = getIntent().getStringExtra(EXTRA_UNIT_PRODUCT);
        stock = getIntent().getIntExtra(EXTRA_STOCK_PRODUCT, 0);
        price = getIntent().getIntExtra(EXTRA_PRICE_PRODUCT, 0);
        idProduct = getIntent().getIntExtra(EXTRA_ID_PRODUCT, 0);

        inputName.setText(name);
        inputUnit.setText(unit);
        inputPrice.setText(String.valueOf(price));
        inputStock.setText(String.valueOf(stock));
    }


    public void updateProduct() {
        getTextInput();
        if (!checkName() || !checkUnit()) {
            Snackbar.make(coordinatorLayout, R.string.input_empty,
                    Snackbar.LENGTH_SHORT)
                    .show();
        } else {
            Product product = new Product(idProduct, stock, name, unit, price);
            productViewModel.updateProduct(product);
            finish();
        }
    }

    private boolean checkName() {
        inputName.setError(null);
        if (name.length() == 0) {
            inputName.setError(getString(R.string.input_empty));
            return false;
        }
        return true;
    }

    private boolean checkUnit() {
        inputUnit.setError(null);
        if (unit.length() == 0) {
            inputUnit.setError(getString(R.string.input_empty));
            return false;
        }
        return true;
    }


    private void getTextInput() {
        name = inputName.getText().toString();
        unit = inputUnit.getText().toString();
        if (inputPrice.getText().toString().equalsIgnoreCase("")) {
            inputPrice.setError("Belum diisi");
        } else {
            price = Integer.parseInt(inputPrice.getText().toString());

        }

        if (inputStock.getText().toString().equalsIgnoreCase("")) {
            inputStock.setError("Belum diisi");
        } else {
            stock = Integer.parseInt(inputStock.getText().toString());

        }
    }

    @OnClick({R.id.btn_update, R.id.btn_cancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_update:
                updateProduct();
                break;
            case R.id.btn_cancel:
                finish();
                break;
        }
    }
}
