package id.qibee.edu.akuntansiukm.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.qibee.edu.akuntansiukm.MainActivity;
import id.qibee.edu.akuntansiukm.R;
import timber.log.Timber;

import static id.qibee.edu.akuntansiukm.util.Constants.AKUNTUKM_PREF;
import static id.qibee.edu.akuntansiukm.util.Constants.USERFIELD;
import static id.qibee.edu.akuntansiukm.util.Constants.USERNAME;

public class ProfilInputActivity extends AppCompatActivity {
    SharedPreferences sharedPreferences;
    @BindView(R.id.input_name)
    TextInputEditText inputName;
    @BindView(R.id.input_field)
    TextInputEditText inputField;
    @BindView(R.id.input_owner)
    TextInputEditText inputOwner;
    @BindView(R.id.input_notes)
    TextInputEditText inputNotes;
    @BindView(R.id.btn_send)
    AppCompatButton btnSend;
    @BindView(R.id.btn_cancel)
    AppCompatButton btnCancel;
    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;
    private String name, field;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil_input);
        ButterKnife.bind(this);
        sharedPreferences = this.getSharedPreferences(AKUNTUKM_PREF, Context.MODE_PRIVATE);

    }

    @OnClick({R.id.btn_send, R.id.btn_cancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_send:
                getTextInput();
                saveProfil();
                break;
            case R.id.btn_cancel:
                finish();
                break;
        }
    }

    private void getTextInput() {
        name = inputName.getText().toString();
        field = inputField.getText().toString();
        Timber.d("name %s", name);
        Timber.d("field %s", field);
    }

    private void saveProfil() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USERNAME, name);
        editor.putString(USERFIELD, field);
        editor.apply();
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
