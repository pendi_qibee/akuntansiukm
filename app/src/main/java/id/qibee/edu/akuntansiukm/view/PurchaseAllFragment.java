package id.qibee.edu.akuntansiukm.view;


import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import id.qibee.edu.akuntansiukm.R;
import id.qibee.edu.akuntansiukm.db.Purchase;
import id.qibee.edu.akuntansiukm.viewmodel.PurchasesViewModel;
import id.qibee.edu.akuntansiukm.viewmodel.TransactionViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class PurchaseAllFragment extends Fragment {


    public static final String EXTRA_ID_PURCHASE = "id_purchase";
    public static final String EXTRA_TRANS_CODE = "code_purchase";
    public static final String EXTRA_PRODUCTNAME_PURCHASE = "product_purchase";
    public static final String EXTRA_VENDOR_PURCHASE = "vendor";
    public static final String EXTRA_CASH_PURCHASE = "cash";
    public static final String EXTRA_DATE_PURCHASE = "date_purchase";
    public static final String EXTRA_NOTE_PURCHASE = "note_purchase";
    @BindView(R.id.rv_month)
    RecyclerView rvMonth;
    @BindView(R.id.rv_purchase)
    RecyclerView rvPurchase;
    @BindView(R.id.fab_add_purchase)
    FloatingActionButton fabAddPurchase;
    Unbinder unbinder;
    @BindView(R.id.empty_state_container)
    LinearLayout emptyStateContainer;

    private PurchasesViewModel purchasesViewModel;
    private TransactionViewModel transactionViewModel;
    private PurchaseRvAdapter purchaseRvAdapter;

    public PurchaseAllFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_purchase_all, container, false);
        unbinder = ButterKnife.bind(this, view);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        DividerItemDecoration decoration = new DividerItemDecoration(view.getContext(), DividerItemDecoration.VERTICAL);
        rvPurchase.addItemDecoration(decoration);
        purchaseRvAdapter = new PurchaseRvAdapter(getContext());
        rvPurchase.setAdapter(purchaseRvAdapter);
        rvPurchase.setLayoutManager(layoutManager);
        purchasesViewModel = ViewModelProviders.of(this).get(PurchasesViewModel.class);
        transactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel.class);
        purchasesViewModel.getAllPurchases().observe(this, purchases -> {
            purchaseRvAdapter.setPurchases(purchases);
            if (purchaseRvAdapter.getItemCount() == 0) {
                emptyStateContainer.setVisibility(View.VISIBLE);
            } else {
                emptyStateContainer.setVisibility(View.GONE);
            }
        });

        // Add the functionality to swipe items in the
        // RecyclerView to delete the swiped item.
        ItemTouchHelper helper = new ItemTouchHelper(
                new ItemTouchHelper.SimpleCallback(0,
                        ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                    @Override
                    // We are not implementing onMove() in this app.
                    public boolean onMove(RecyclerView recyclerView,
                                          RecyclerView.ViewHolder viewHolder,
                                          RecyclerView.ViewHolder target) {
                        return false;
                    }

                    @Override
                    // When the use swipes a word,
                    // delete that word from the database.
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                        int position = viewHolder.getAdapterPosition();
                        Purchase purchase = purchaseRvAdapter.getPurchasesAtPosition(position);
                        Toast.makeText(getContext(),
                                getString(R.string.delete_word_preamble) + " " +
                                        purchase.getVendorName(), Toast.LENGTH_LONG).show();

                        // Delete the word.
                        purchasesViewModel.deletePurchases(purchase);
                        String transactionCode = purchase.getTransactionCode();
                        transactionViewModel.deleteByCode(transactionCode);
                    }
                });
        // Attach the item touch helper to the recycler view.
        helper.attachToRecyclerView(rvPurchase);

        purchaseRvAdapter.setOnItemClickListener((v, position) -> {
            Purchase purchase = purchaseRvAdapter.getPurchasesAtPosition(position);
            PurchaseAllFragment.this.launchUpdateActivity(purchase);
        });

        return view;
    }

    private void launchUpdateActivity(Purchase purchase) {
        List<Purchase> purchaseList = new ArrayList<>();

        Intent intent = new Intent(getContext(), PurchaseUpdateActivity.class);
        intent.putExtra(EXTRA_ID_PURCHASE, purchase.getId());
        intent.putExtra(EXTRA_TRANS_CODE, purchase.getTransactionCode());
        intent.putExtra(EXTRA_PRODUCTNAME_PURCHASE, purchase.getAllProductName());
        intent.putExtra(EXTRA_VENDOR_PURCHASE, purchase.getVendorName());
        intent.putExtra(EXTRA_CASH_PURCHASE, purchase.getTotalCash());
        intent.putExtra(EXTRA_DATE_PURCHASE, purchase.getPurchaseDate());
        intent.putExtra(EXTRA_NOTE_PURCHASE, purchase.getNotes());
        startActivity(intent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.fab_add_purchase)
    public void onViewClicked() {
        startActivity(new Intent(getContext(), PurchaseInputActivity.class));

    }
}
