package id.qibee.edu.akuntansiukm.view;

import android.app.DatePickerDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.qibee.edu.akuntansiukm.R;
import id.qibee.edu.akuntansiukm.db.Purchase;
import id.qibee.edu.akuntansiukm.db.Transaction;
import id.qibee.edu.akuntansiukm.util.Constants;
import id.qibee.edu.akuntansiukm.util.Helper;
import id.qibee.edu.akuntansiukm.viewmodel.PurchasesViewModel;
import id.qibee.edu.akuntansiukm.viewmodel.TransactionViewModel;

public class PurchaseInputActivity extends AppCompatActivity {

    @BindView(R.id.input_date)
    TextInputEditText inputDate;
    @BindView(R.id.input_name)
    TextInputEditText inputName;
    @BindView(R.id.input_total_cash)
    TextInputEditText inputTotalCash;
    @BindView(R.id.input_nama_vendor)
    TextInputEditText inputNamaVendor;
    @BindView(R.id.input_catatan)
    TextInputEditText inputCatatan;
    @BindView(R.id.btn_send)
    AppCompatButton btnSend;
    @BindView(R.id.btn_cancel)
    AppCompatButton btnCancel;
    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    private int mYear, mMonth, mDay, mHour, mMinute;
    private String productName, vendorName, notes, totalCashString;
    private int totalCash = 0;
    private Date purchaseDate;
    private TransactionViewModel transactionViewModel;
    private PurchasesViewModel purchasesViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Stetho.initializeWithDefaults(this);
        setContentView(R.layout.activity_purchase_input);
        ButterKnife.bind(this);

        Date c = Calendar.getInstance().getTime();
//        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat timestamp = new SimpleDateFormat(Constants.TIME_STAMP_FORMAT);
        String formattedDate = df.format(c);
        inputDate.setText(formattedDate);
        purchaseDate = c; //hasil Thu Jul 12 11:16:08 GMT+07:00 2018
//        String timeStamp = timestamp.format(c); //hasil 2018-07-12 11:16:08
//        Log.d(Constants.TAG, "onCreate: purchaseDate" + purchaseDate);
//        Log.d(Constants.TAG, "onCreate: formatTime" + timeStamp);

        purchasesViewModel = ViewModelProviders.of(this).get(PurchasesViewModel.class);
        transactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel.class);

    }

    @OnClick({R.id.input_date, R.id.btn_send, R.id.btn_cancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.input_date:
                setDate();
                break;
            case R.id.btn_send:
                savePurchase();
                break;
            case R.id.btn_cancel:
                finish();
                break;
        }
    }

    private boolean checkProductName() {
        inputName.setError(null);
        if (productName.length() == 0) {
            inputName.setError(getString(R.string.input_empty));
            return false;
        }
        return true;
    }

    private boolean checkVendorName() {
        inputNamaVendor.setError(null);
        if (vendorName.length() == 0) {
            inputNamaVendor.setError(getString(R.string.input_empty));
            return false;
        }
        return true;
    }

    private boolean checkTotalCash() {
        inputTotalCash.setError(null);
        if (totalCashString.length() == 0) {
            inputTotalCash.setError(getString(R.string.input_empty));
            return false;
        }
        return true;
    }

    private void getTextInput() {
        productName = inputName.getText().toString();
        vendorName = inputNamaVendor.getText().toString();
        totalCashString = inputTotalCash.getText().toString();
        notes = inputCatatan.getText().toString();
    }

    private void savePurchase() {
        getTextInput();
        if (!checkProductName() || !checkVendorName() || !checkTotalCash()) {
            Snackbar.make(coordinatorLayout, R.string.input_not_complete,
                    Snackbar.LENGTH_SHORT)
                    .show();
        } else {
            totalCash=Integer.parseInt(totalCashString);
            String transactionCode = String.valueOf(System.currentTimeMillis());
            Purchase purchase = new Purchase(transactionCode, totalCash, productName, notes, vendorName, purchaseDate);
            purchasesViewModel.insertPurchases(purchase);

            Transaction transaction = new Transaction(transactionCode, "Pembelian", productName, 0, totalCash, purchaseDate );
            transactionViewModel.insertTransaction(transaction);
            finish();
        }
    }

    private void setDate() {
// Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                (view, year, monthOfYear, dayOfMonth) -> {
                    inputDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    purchaseDate = Helper.getDateFromDatePicker(year, monthOfYear, dayOfMonth);
//                    Log.d(Constants.TAG, "onDateSet: " + purchaseDate);
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }
}
