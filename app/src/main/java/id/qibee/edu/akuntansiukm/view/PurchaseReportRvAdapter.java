package id.qibee.edu.akuntansiukm.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.qibee.edu.akuntansiukm.R;
import id.qibee.edu.akuntansiukm.db.Purchase;
import id.qibee.edu.akuntansiukm.util.Helper;
import timber.log.Timber;

/**
 * Adapter for the RecyclerView that displays a list of words.
 */

public class PurchaseReportRvAdapter extends RecyclerView.Adapter<PurchaseReportRvAdapter.PurchasesViewHolder> {

    private final LayoutInflater mInflater;
    private List<Purchase> purchaseList; // Cached copy of words
    private static ClickListener clickListener;

    PurchaseReportRvAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public PurchasesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_purchases_report, parent, false);
        return new PurchasesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PurchasesViewHolder holder, int position) {
        if (purchaseList != null) {
            Purchase current = purchaseList.get(position);
            Timber.d("onBindViewHolder: %s", current.getId());
            holder.textItemName.setText(current.getAllProductName());
            holder.textPurchaseDate.setText(Helper.getStringFromDate(current.getPurchaseDate()));
            holder.textTotalCash.setText(Helper.formatRupiah(current.getTotalCash()));

        } else {
            // Covers the case of data not being ready yet.
            holder.textItemName.setText("Belum Ada Data");

        }
    }

    /**
     * Associates a list of words with this adapter
     */
    void setPurchases(List<Purchase> Purchase) {
        purchaseList = Purchase;
        notifyDataSetChanged();
    }

    /**
     * getItemCount() is called many times, and when it is first called,
     * purchaseList has not been updated (means initially, it's null, and we can't return null).
     */
    @Override
    public int getItemCount() {
        if (purchaseList != null)
            return purchaseList.size();
        else return 0;
    }


    public Purchase getPurchasesAtPosition(int position) {
        return purchaseList.get(position);
    }

    class PurchasesViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_purchase_date)
        TextView textPurchaseDate;
        @BindView(R.id.text_item_name)
        TextView textItemName;
        @BindView(R.id.text_total_cash)
        TextView textTotalCash;
        @BindView(R.id.card_task)
        LinearLayout cardTask;
        private PurchasesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(view -> clickListener.onItemClick(view, getAdapterPosition()));
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        PurchaseReportRvAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(View v, int position);
    }

}
