

package id.qibee.edu.akuntansiukm.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.qibee.edu.akuntansiukm.R;
import id.qibee.edu.akuntansiukm.db.Purchase;
import id.qibee.edu.akuntansiukm.util.Helper;
import timber.log.Timber;

/**
 * Adapter for the RecyclerView that displays a list of words.
 */

public class PurchaseRvAdapter extends RecyclerView.Adapter<PurchaseRvAdapter.PurchasesViewHolder> {

    private final LayoutInflater mInflater;


    private List<Purchase> purchaseList; // Cached copy of words
    private static ClickListener clickListener;

    PurchaseRvAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public PurchasesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_purchases, parent, false);
        return new PurchasesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PurchasesViewHolder holder, int position) {
        if (purchaseList != null) {
            Purchase current = purchaseList.get(position);
            Timber.d("onBindViewHolder: %s", current.getId());
            holder.textProductName.setText(current.getAllProductName());
            holder.textVendor.setText(current.getVendorName());
            holder.textTotalCash.setText(Helper.formatRupiah(current.getTotalCash()));

        } else {
            // Covers the case of data not being ready yet.
            holder.textProductName.setText("Belum Ada Data");

        }
    }

    /**
     * Associates a list of words with this adapter
     */
    void setPurchases(List<Purchase> Purchase) {
        purchaseList = Purchase;
        notifyDataSetChanged();
    }

    /**
     * getItemCount() is called many times, and when it is first called,
     * purchaseList has not been updated (means initially, it's null, and we can't return null).
     */
    @Override
    public int getItemCount() {
        if (purchaseList != null)
            return purchaseList.size();
        else return 0;
    }


    public Purchase getPurchasesAtPosition(int position) {
        return purchaseList.get(position);
    }

    class PurchasesViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_product_name)
        TextView textProductName;
        @BindView(R.id.text_vendor)
        TextView textVendor;
        @BindView(R.id.text_total_cash)
        TextView textTotalCash;
        private PurchasesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(view -> clickListener.onItemClick(view, getAdapterPosition()));
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        PurchaseRvAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(View v, int position);
    }

}
