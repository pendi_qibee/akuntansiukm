package id.qibee.edu.akuntansiukm.view;

import android.app.DatePickerDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.DatePicker;

import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.qibee.edu.akuntansiukm.R;
import id.qibee.edu.akuntansiukm.db.Purchase;
import id.qibee.edu.akuntansiukm.util.Helper;
import id.qibee.edu.akuntansiukm.viewmodel.PurchasesViewModel;

public class PurchaseUpdateActivity extends AppCompatActivity {
    public static final String EXTRA_ID_PURCHASE = "id_purchase";
    public static final String EXTRA_TRANS_CODE = "code_purchase";
    public static final String EXTRA_PRODUCTNAME_PURCHASE = "product_purchase";
    public static final String EXTRA_VENDOR_PURCHASE = "vendor";
    public static final String EXTRA_CASH_PURCHASE = "cash";
    public static final String EXTRA_DATE_PURCHASE = "date_purchase";
    public static final String EXTRA_NOTE_PURCHASE = "note_purchase";
    @BindView(R.id.input_date)
    TextInputEditText inputDate;
    @BindView(R.id.input_name)
    TextInputEditText inputName;
    @BindView(R.id.input_total_cash)
    TextInputEditText inputTotalCash;
    @BindView(R.id.input_nama_vendor)
    TextInputEditText inputNamaVendor;
    @BindView(R.id.input_catatan)
    TextInputEditText inputCatatan;
    @BindView(R.id.btn_send)
    AppCompatButton btnSend;
    @BindView(R.id.btn_cancel)
    AppCompatButton btnCancel;
    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;
    private int idPurchase, mYear, mMonth, mDay, mHour, mMinute;
    private String productName, vendorName, notes, dateString, transactionCode;
    private int totalCash = 0;
    private Date purchaseDate;
    private Purchase purchase;
    private PurchasesViewModel purchasesViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_update);
        ButterKnife.bind(this);
//Stetho.initializeWithDefaults(this);

        idPurchase = getIntent().getIntExtra(EXTRA_ID_PURCHASE, 0);
        totalCash = getIntent().getIntExtra(EXTRA_CASH_PURCHASE, 0);
        transactionCode = getIntent().getStringExtra(EXTRA_TRANS_CODE);
        productName = getIntent().getStringExtra(EXTRA_PRODUCTNAME_PURCHASE);
        vendorName = getIntent().getStringExtra(EXTRA_VENDOR_PURCHASE);
        notes = getIntent().getStringExtra(EXTRA_NOTE_PURCHASE);

        purchaseDate = (Date) getIntent().getSerializableExtra(EXTRA_DATE_PURCHASE);


        setInputContent();
        purchasesViewModel = ViewModelProviders.of(this).get(PurchasesViewModel.class);

    }

    private void setInputContent() {

        if (purchaseDate != null) {
            inputDate.setText(Helper.getStringFromDate(purchaseDate));
        }
        inputName.setText(productName);
        inputNamaVendor.setText(vendorName);
        inputTotalCash.setText(String.valueOf(totalCash));
        inputCatatan.setText(notes);

    }

    @OnClick({R.id.input_date, R.id.btn_send, R.id.btn_cancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.input_date:
                setDate();
                break;
            case R.id.btn_send:
                savePurchase();
                break;
            case R.id.btn_cancel:
                finish();
                break;
        }
    }


    private boolean checkProductName() {
        inputName.setError(null);
        if (productName.length() == 0) {
            inputName.setError(getString(R.string.input_empty));
            return false;
        }
        return true;
    }

    private boolean checkVendorName() {
        inputNamaVendor.setError(null);
        if (vendorName.length() == 0) {
            inputNamaVendor.setError(getString(R.string.input_empty));
            return false;
        }
        return true;
    }

    private boolean checkTotalCash() {
        inputTotalCash.setError(null);
        if (totalCash == 0) {
            inputTotalCash.setError(getString(R.string.input_empty));
            return false;
        }
        return true;
    }


    private void getTextInput() {
        productName = inputName.getText().toString();
        vendorName = inputNamaVendor.getText().toString();
        totalCash = Integer.parseInt(inputTotalCash.getText().toString());
        notes = inputCatatan.getText().toString();
    }

    private void savePurchase() {
        getTextInput();
        if (!checkProductName() || !checkVendorName() || !checkTotalCash()) {
            Snackbar.make(coordinatorLayout, R.string.input_empty,
                    Snackbar.LENGTH_SHORT)
                    .show();
        } else {
            Purchase purchase = new Purchase(idPurchase, transactionCode, totalCash, productName, notes, vendorName, purchaseDate);
            purchasesViewModel.updatePurchases(purchase);
            finish();
        }
    }

    private void setDate() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                (DatePicker view, int year, int monthOfYear, int dayOfMonth) -> {
                    inputDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    purchaseDate = Helper.getDateFromDatePicker(year, monthOfYear, dayOfMonth);
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }
}
