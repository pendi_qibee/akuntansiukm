package id.qibee.edu.akuntansiukm.view;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import id.qibee.edu.akuntansiukm.QibeeApp;
import id.qibee.edu.akuntansiukm.R;
import id.qibee.edu.akuntansiukm.util.Helper;
import id.qibee.edu.akuntansiukm.viewmodel.ExpenseViewModel;
import id.qibee.edu.akuntansiukm.viewmodel.PurchasesViewModel;
import id.qibee.edu.akuntansiukm.viewmodel.SalesViewModel;
import timber.log.Timber;


public class RevenueAllFragment extends Fragment {
    @BindView(R.id.text_sales_value)
    TextView textSalesValue;
    @BindView(R.id.text_purchase_value)
    TextView textPurchaseValue;
    @BindView(R.id.text_expense_value)
    TextView textExpenseValue;
    Unbinder unbinder;
    //    @BindView(R.id.text_revenue)
//    TextView textRevenue;
    @BindView(R.id.text_name)
    TextView textName;
    @BindView(R.id.text_bussiness_field)
    TextView textBussinessField;
    @BindView(R.id.image_edit)
    ImageView imageEdit;
    @BindView(R.id.card_rekap_bulanan)
    CardView cardRekapBulanan;
    @BindView(R.id.card_labarugi)
    CardView cardLabarugi;
    @BindView(R.id.card_transaksi)
    CardView cardTransaksi;
    @BindView(R.id.text_menu_rekap)
    TextView textMenuRekap;
    @BindView(R.id.image_arrow)
    ImageView imageArrow;
    @BindView(R.id.image_app_guide)
    ImageView imageAppGuide;
    @BindView(R.id.text_menu_labarugi)
    TextView textMenuLabarugi;
    @BindView(R.id.image_arrow2)
    ImageView imageArrow2;
    @BindView(R.id.text_menu_detail)
    TextView textMenuDetail;
    @BindView(R.id.image_arrow3)
    ImageView imageArrow3;
    @BindView(R.id.text_menu_penjualan)
    TextView textMenuPenjualan;
    @BindView(R.id.image_arrow4)
    ImageView imageArrow4;
    @BindView(R.id.card_sales)
    CardView cardSales;
    @BindView(R.id.text_menu_pembelian)
    TextView textMenuPembelian;
    @BindView(R.id.image_arrow6)
    ImageView imageArrow6;
    @BindView(R.id.card_purchase)
    CardView cardPurchase;
    @BindView(R.id.text_menu_product)
    TextView textMenuProduct;
    @BindView(R.id.image_arrow5)
    ImageView imageArrow5;
    @BindView(R.id.card_product)
    CardView cardProduct;
    @BindView(R.id.text_menu_expense)
    TextView textMenuExpense;
    @BindView(R.id.image_arrow7)
    ImageView imageArrow7;
    @BindView(R.id.card_expense)
    CardView cardExpense;

    private SalesViewModel salesViewModel;
    private PurchasesViewModel purchasesViewModel;
    private ExpenseViewModel expenseViewModel;

    private String name, field;

    private OnItemSelectedListener listener;

    public RevenueAllFragment() {
        // Required empty public constructor
    }

    public interface OnItemSelectedListener {
        public void onCardClick(String tag);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_revenue_all, container, false);
        unbinder = ButterKnife.bind(this, view);

        salesViewModel = ViewModelProviders.of(this).get(SalesViewModel.class);

        purchasesViewModel = ViewModelProviders.of(this).get(PurchasesViewModel.class);
        expenseViewModel = ViewModelProviders.of(this).get(ExpenseViewModel.class);

        name = QibeeApp.getInstance().getUsername();
        field = QibeeApp.getInstance().getUserField();

        if (name == null) {
            textName.setText("NAMA USAHA ANDA");
            textBussinessField.setText("BIDANG USAHA");
        } else {
            textName.setText(name);
            textBussinessField.setText(field);

        }
        Timber.d("nama: %s", name);
        Timber.d("bidang: %s", field);


        getAllValue();

        return view;


    }

    @SuppressLint("StaticFieldLeak")
    private void getAllValue() {
        final int[] totalSales = {0};
        final int[] totalPurchases = {0};
        final int[] totalExpenses = {0};
        final int[] totalRevenue = {0};
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                if (salesViewModel.getTotal().getTotal() != 0) {
                    totalSales[0] = salesViewModel.getTotal().getTotal();
                }

                if (purchasesViewModel.getTotal().getTotal() != 0) {
                    totalPurchases[0] = purchasesViewModel.getTotal().getTotal();
                }

                if (expenseViewModel.getTotal().getTotal() != 0) {
                    totalExpenses[0] = expenseViewModel.getTotal().getTotal();
                }

                totalRevenue[0] = totalSales[0] - totalPurchases[0] - totalExpenses[0];

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (textSalesValue != null) {
                    textSalesValue.setText(Helper.formatRupiah(totalSales[0]));
                }
                if (textPurchaseValue != null) {
                    textPurchaseValue.setText(Helper.formatRupiah(totalPurchases[0]));
                }
                if (textExpenseValue != null) {
                    textExpenseValue.setText(Helper.formatRupiah(totalExpenses[0]));
                }
//                if (textRevenue != null) {
//                    textRevenue.setText("Total Keuntungan\n" + Helper.formatRupiah(totalRevenue[0]));
//                }

//                textExpenseValue.setText(Helper.formatRupiah((expenseViewModel.getTotalCashIn().getTotalCashIn())));
//                textPurchaseValue.setText(Helper.formatRupiah((purchasesViewModel.getTotalCashIn().getTotalCashIn())));
            }
        }.execute();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.image_edit)
    public void onViewClicked() {
        launchProfilDialog();
        Timber.d("edit clicked");
    }

    private void launchProfilDialog() {

        startActivity(new Intent(getContext(), ProfilInputActivity.class));
    }

    @OnClick({R.id.card_rekap_bulanan, R.id.card_labarugi, R.id.card_transaksi, R.id.card_sales,
            R.id.card_purchase, R.id.card_expense, R.id.card_product, R.id.image_app_guide})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.card_rekap_bulanan:
                listener.onCardClick("rekap_bulanan");
                break;
            case R.id.card_labarugi:
                listener.onCardClick("labarugi");
                break;
            case R.id.card_transaksi:
                listener.onCardClick("transaksi");
                break;
            case R.id.card_sales:
                listener.onCardClick("penjualan");
                break;
            case R.id.card_purchase:
                listener.onCardClick("pembelian");
                break;
            case R.id.card_expense:
                listener.onCardClick("pengeluaran");
                break;
            case R.id.card_product:
                listener.onCardClick("produk");
                break;
                case R.id.image_app_guide:
                getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://drive.google.com/open?id=18phOd7peV6T52DnsaL8au_vwzlc5t3nz")));
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnItemSelectedListener) {
            listener = (OnItemSelectedListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement MyListFragment.OnItemSelectedListener");
        }
    }
}

