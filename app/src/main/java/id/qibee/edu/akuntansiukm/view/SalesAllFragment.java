package id.qibee.edu.akuntansiukm.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import id.qibee.edu.akuntansiukm.R;
import id.qibee.edu.akuntansiukm.db.Sales;
import id.qibee.edu.akuntansiukm.viewmodel.CartViewModel;
import id.qibee.edu.akuntansiukm.viewmodel.SalesViewModel;


public class SalesAllFragment extends Fragment {
    // Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "SalesFragment";
    @BindView(R.id.rv_sales)
    RecyclerView rvSales;
    @BindView(R.id.fab_add_sales)
    FloatingActionButton fabAddSales;
    Unbinder unbinder;
    @BindView(R.id.rv_month)
    RecyclerView rvMonth;
    @BindView(R.id.empty_state_container)
    LinearLayout emptyStateContainer;
    private SalesViewModel salesViewModel;
    private CartViewModel cartViewModel;
    private SalesRvAdapter salesRvAdapter;
    private MonthRvAdapter monthRvAdapter;
    // Rename and change types of parameters
    private String mParam1;
    private String mParam2;

//    private OnFragmentInteractionListener mListener;

    public SalesAllFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sales_all, container, false);
        unbinder = ButterKnife.bind(this, view);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        RecyclerView.LayoutManager layoutManagerMonth = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, true);
        DividerItemDecoration decoration = new DividerItemDecoration(view.getContext(), DividerItemDecoration.VERTICAL);
        rvSales.addItemDecoration(decoration);
        salesRvAdapter = new SalesRvAdapter(getContext());
        monthRvAdapter = new MonthRvAdapter(getContext());
        rvSales.setAdapter(salesRvAdapter);
        rvMonth.setAdapter(monthRvAdapter);
        rvSales.setLayoutManager(layoutManager);
        rvMonth.setLayoutManager(layoutManagerMonth);
        salesViewModel = ViewModelProviders.of(this).get(SalesViewModel.class);
        cartViewModel = ViewModelProviders.of(this).get(CartViewModel.class);
        salesViewModel.getMonth().observe(this, sales -> monthRvAdapter.setSales(sales));
        salesViewModel.getAllSaleses().observe(this, sales -> {
            salesRvAdapter.setSales(sales);
            if (salesRvAdapter.getItemCount() == 0) {
                emptyStateContainer.setVisibility(View.VISIBLE);
            } else {
                emptyStateContainer.setVisibility(View.GONE);
            }
        });

        ItemTouchHelper helper = new ItemTouchHelper(
                new ItemTouchHelper.SimpleCallback(0,
                        ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                    @Override
                    // We are not implementing onMove() in this app.
                    public boolean onMove(RecyclerView recyclerView,
                                          RecyclerView.ViewHolder viewHolder,
                                          RecyclerView.ViewHolder target) {
                        return false;
                    }

                    @Override
                    // When the use swipes a word,
                    // delete that word from the database.
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                        int position = viewHolder.getAdapterPosition();
                        Sales sales = salesRvAdapter.getSalesAtPosition(position);
//                        Toast.makeText(getContext(),
//                                getString(R.string.delete_word_preamble) + " " +
//                                        sales.getId(), Toast.LENGTH_LONG).show();

                        int idTransaction = sales.getId();
                        salesViewModel.deleteSales(sales);

                        cartViewModel.deleteCartWithId(idTransaction);
                    }
                });
        // Attach the item touch helper to the recycler view.
        helper.attachToRecyclerView(rvSales);

        salesRvAdapter.setOnItemClickListener((v, position) -> {
            Sales sales = salesRvAdapter.getSalesAtPosition(position);
//            launchDetailSalesActivity(sales);
        });
        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.fab_add_sales)
    public void onViewClicked() {
        startActivity(new Intent(getContext(), SalesInputActivity.class));
    }

}
