package id.qibee.edu.akuntansiukm.view;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.qibee.edu.akuntansiukm.R;
import id.qibee.edu.akuntansiukm.db.Cart;
import id.qibee.edu.akuntansiukm.db.Product;
import id.qibee.edu.akuntansiukm.db.Sales;
import id.qibee.edu.akuntansiukm.db.TotalCash;
import id.qibee.edu.akuntansiukm.db.Transaction;
import id.qibee.edu.akuntansiukm.util.Helper;
import id.qibee.edu.akuntansiukm.viewmodel.CartViewModel;
import id.qibee.edu.akuntansiukm.viewmodel.ProductViewModel;
import id.qibee.edu.akuntansiukm.viewmodel.SalesViewModel;
import id.qibee.edu.akuntansiukm.viewmodel.TransactionViewModel;
import timber.log.Timber;

public class SalesInputActivity extends AppCompatActivity {
    public static final String EXTRA_TRANSACTION_ID = "trans_id";
    public static final String EXTRA_DATE = "sales_date";
    private static final String TAG = "sales list";
    @BindView(R.id.input_date)
    TextInputEditText inputDate;
    @BindView(R.id.rv_cart)
    RecyclerView rvCart;
    @BindView(R.id.tv_add_cart)
    TextView tvAddCart;
    @BindView(R.id.lyt_add_cart)
    MaterialRippleLayout lytAddCart;
    @BindView(R.id.coordinator_layout)
    LinearLayout coordinatorLayout;
    @BindView(R.id.layout_button_save)
    LinearLayout layoutButtonAdd;
    @BindView(R.id.button_add)
    Button buttonAdd;
    @BindView(R.id.text_total)
    TextView textTotal;
    private DatePickerDialog mDatePickerDialog;
    private Calendar mCalendarCurrentDate;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private Date salesDate;
    private CartRvAdapter cartRvAdapter;
    private CartViewModel cartViewModel;
    private SalesViewModel salesViewModel;
    private TransactionViewModel transactionViewModel;
    private ProductViewModel productViewModel;
    int lastIdSales;
    private String productSold;
    private int totalCash;
    private int transactionId = 1;
    private int transactionIdNew;

    private Cart cart;
    private List<Cart> cartList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Stetho.initializeWithDefaults(this);
        setContentView(R.layout.activity_sales_input);
        ButterKnife.bind(this);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        DividerItemDecoration decoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        rvCart.addItemDecoration(decoration);
        cartRvAdapter = new CartRvAdapter(this);
        rvCart.setAdapter(cartRvAdapter);
        rvCart.setLayoutManager(layoutManager);
        salesViewModel = ViewModelProviders.of(this).get(SalesViewModel.class);
        salesViewModel.getmMaxTransId().observe(this, this::logsalesModel);
        transactionIdNew = getIntent().getIntExtra(EXTRA_TRANSACTION_ID, 0);
        transactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel.class);
        cartViewModel = ViewModelProviders.of(this).get(CartViewModel.class);
//        cartViewModel.getCurrentCart(transactionId).observe(this, new Observer<List<Cart>>() {
        cartViewModel.getCurrentCart(transactionIdNew).observe(this, carts -> {
//            Log.d(TAG, "onCreate: transaksi id" + transactionIdNew);
//            Log.d(TAG, "onCreate: cart size" + carts.size());
            cartRvAdapter.setCartList(carts);
            cartRvAdapter.notifyDataSetChanged();
        });

        productViewModel = ViewModelProviders.of(this).get(ProductViewModel.class);
        getTotals();


        // Add the functionality to swipe items in the
        // RecyclerView to delete the swiped item.
        ItemTouchHelper helper = new ItemTouchHelper(
                new ItemTouchHelper.SimpleCallback(0,
                        ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                    @Override
                    // We are not implementing onMove() in this app.
                    public boolean onMove(RecyclerView recyclerView,
                                          RecyclerView.ViewHolder viewHolder,
                                          RecyclerView.ViewHolder target) {
                        return false;
                    }

                    @Override
                    // When the use swipes a word,
                    // delete that word from the database.
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                        int position = viewHolder.getAdapterPosition();
                        Cart cart = cartRvAdapter.getCartAtPosition(position);
//                        Toast.makeText(SalesInputActivity.this,
//                                getString(R.string.delete_word_preamble) + " " +
//                                        cart.getTransaction_id(), Toast.LENGTH_LONG).show();

                        // Delete the word.
                        cartViewModel.deleteCart(cart);
                        getTotals();
                    }
                });
        // Attach the item touch helper to the recycler view.
        helper.attachToRecyclerView(rvCart);

        cartRvAdapter.setOnItemClickListener((v, position) -> {
            Cart cart = cartRvAdapter.getCartAtPosition(position);
            launchUpdateDialog(cart);
        });

        if (savedInstanceState != null) {
            salesDate = (Date) savedInstanceState.getSerializable("date");
            if (salesDate != null) {
                inputDate.setText(Helper.getStringFromDate(salesDate));
            }
            Timber.d("salesdate..%s", salesDate);
        }

        salesDate = (Date) getIntent().getSerializableExtra(EXTRA_DATE);
        Timber.d("salesdate from intent..%s", salesDate);

        if (salesDate != null) {
            inputDate.setText(Helper.getStringFromDate(salesDate));
        } else {
            Date c = Calendar.getInstance().getTime();
            inputDate.setText(Helper.getStringFromDate(c));
            salesDate = c;

        }


    }

    private void launchUpdateDialog(Cart cart) {
        final int[] stock = new int[0];
        final int[] productStock = {cart.getStock()};
        final int[] cartQuantity = {cart.getQuantity()};
        //failed SalesInputActivity$$Lambda$3.onChanged(Unknown Source)
        /*
        productViewModel.getProductWithId(cart.getPoductId()).observe(this, product -> {
                    if (product!=null){
                        stock[0] =product.getStock();
                        Log.d(TAG, "launchUpdateDialog: stock[0] ="+stock);
                    }
                });
        */

        final Dialog dialog = new Dialog(SalesInputActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_cart_option);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        ((TextView) dialog.findViewById(R.id.title)).setText(cart.getPoductName());
        ((TextView) dialog.findViewById(R.id.stock)).setText(getString(R.string.stock) + productStock[0]);
        final TextView qty = dialog.findViewById(R.id.quantity);
        qty.setText(cartQuantity[0] + "");

        dialog.findViewById(R.id.img_decrease).setOnClickListener(view -> {

            if (cartQuantity[0] > 0) {
                cartQuantity[0] = cartQuantity[0] - 1;
                qty.setText(cartQuantity[0] + "");

//                int productStockTemp =productStock[0]
                int productStockTemp = productStock[0] - cartQuantity[0];
                ((TextView) dialog.findViewById(R.id.stock)).setText(getString(R.string.stock) + productStockTemp);

//                Log.d(TAG, "launchUpdateDialog: decrease stock<=" + productStock[0]);

            }
        });
        dialog.findViewById(R.id.img_increase).setOnClickListener(view -> {
            if (cartQuantity[0] <= productStock[0]) {
                cartQuantity[0] += 1;
                qty.setText(cartQuantity[0] + "");

                int productStockTemp = productStock[0] - cartQuantity[0];
                ((TextView) dialog.findViewById(R.id.stock)).setText(getString(R.string.stock) + productStockTemp);

//                Log.d(TAG, "launchUpdateDialog: increase stock<=" + productStock[0]);
            }

        });
        dialog.findViewById(R.id.bt_save).setOnClickListener(view -> {

            int subTotalUpdate = cartQuantity[0] * cart.getPrice();
            cart.setQuantity(cartQuantity[0]);
            cart.setSubTotal(subTotalUpdate);
            cartViewModel.updateCart(cart);
            dialog.dismiss();
            recreate();
        });
        dialog.findViewById(R.id.bt_remove).setOnClickListener(view -> dialog.dismiss());
        dialog.show();
        dialog.getWindow().setAttributes(lp);
//
//        Intent intent = new Intent(this, SalesUpdateActivity.class);
////        intent.putExtra(EXTRA_ID_CART, cart.getId());
////        intent.putExtra(EXTRA_QUANTITY, cart.getQuantity());
////        intent.putExtra(EXTRA_QUANTITY, cart.get());
//        startActivity(intent);


    }

    @SuppressLint("StaticFieldLeak")
    private void getTotals() {
        final int[] totalan = {0};
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                TotalCash totalCash = null;
                Timber.d("doInBackground: %s", cartViewModel.getTotalCart(transactionIdNew));
                if (cartViewModel.getTotalCart(transactionIdNew) != null) {
                    int totals;
                    totals = cartViewModel.getTotalCart(transactionIdNew).getTotal();
//                    Log.d(TAG, "doInBackground: " + totals);
                    totalan[0] = totals;
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                textTotal.setText(Helper.formatRupiah(totalan[0]));
            }
        }.execute();

    }

    private void saveCarts(List<Cart> carts) {
        StringBuilder sb = new StringBuilder();
        int totalCash = 0;
        if (carts.size() != 0) {
            lastIdSales = carts.get(0).getId();
            String isi = carts.get(0).getPoductName();
//            Log.w("logsales", "logsalesModel: " + isi);

            List<String> listProductSold = new ArrayList<>();
            for (Cart mCart : carts) {
                listProductSold.add(mCart.getPoductName());
                sb.append(mCart.getPoductName()).append(", ");
                totalCash = totalCash + mCart.getSubTotal();

                int idProduct = mCart.getPoductId();
                int quantity = mCart.getQuantity();

//work : getstock value
                final int[] stock = new int[1];
                productViewModel.getProductWithId(idProduct).observe(this, new Observer<Product>() {
                    @Override
                    public void onChanged(@Nullable Product product) {
                        if (product != null) {
                            stock[0] = product.getStock();
                            stock[0] -= quantity;
                            updateStock(idProduct, stock);
//                            String productName = product.getName();
//                            Log.d(TAG, "onChanged: name=" + productName);
//                            Log.d(TAG, "saveCarts: stock=" + stock[0]);
                        }
                    }

                    private void updateStock(int idProduct, int[] stock) {
                        productViewModel.updateStockbyId(idProduct, stock);
//                        Log.d(TAG, "updateStock: idProduct = " + idProduct);
//                        Log.d(TAG, "updateStock: stock = " + stock[0]);
                    }

                });
//                Log.d(TAG, "saveCarts: id=" + idProduct);
            }
            String products = sb.toString();
            String transactionCode = String.valueOf(System.currentTimeMillis());
            Sales sales = new Sales(transactionIdNew, transactionCode, products, totalCash, salesDate);
            salesViewModel.insertSales(sales);

            //todo input dengan kode transaksi
            Transaction transaction = new Transaction(transactionCode, "Penjualan", products, totalCash, 0, salesDate );
            transactionViewModel.insertTransaction(transaction);
//            Log.w("SAlesinputAct", "product sold: " + products);
//            Log.w("SAlesinputAct", "total cash: " + totalCash);


        }
    }

    @OnClick({R.id.input_date, R.id.lyt_add_cart, R.id.layout_button_save, R.id.button_add})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.input_date:
                setDate();
                break;
            case R.id.layout_button_save:
                cartViewModel.getCurrentCart(transactionIdNew).observe(this, carts -> {
                    if (carts != null) {
                        SalesInputActivity.this.saveCarts(carts);
                        finish();
                    }
                });
                break;
            case R.id.button_add:
                Intent intent = new Intent(SalesInputActivity.this, CartActivity.class);
                intent.putExtra(EXTRA_TRANSACTION_ID, transactionId);
                intent.putExtra(EXTRA_DATE, salesDate);
                startActivity(intent);
                finish();
                break;
        }
    }

    private void setDate() {
// Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                (view, year, monthOfYear, dayOfMonth) -> {
                    inputDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    salesDate = Helper.getDateFromDatePicker(year, monthOfYear, dayOfMonth);
                }, mYear, mMonth, mDay);
        datePickerDialog.show();

    }


    public int logsalesModel(List<Sales> sales) {
        if (sales.size() != 0) {
            transactionId = sales.get(0).getId() + 1;
            return transactionId;
        }
        return 1;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (cartList.size() != 0) {
            for (Cart mCart : cartList) {
                cartViewModel.deleteCart(mCart);
            }
        }

        finish();
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putSerializable("date", salesDate);
        Timber.d("date outstate..%s", salesDate);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}
