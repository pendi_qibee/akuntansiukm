/*
 * Copyright (C) 2018 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package id.qibee.edu.akuntansiukm.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.qibee.edu.akuntansiukm.R;
import id.qibee.edu.akuntansiukm.db.Sales;
import id.qibee.edu.akuntansiukm.util.Helper;

/**
 * Adapter for the RecyclerView that displays a list of words.
 */

public class SalesReportRvAdapter extends RecyclerView.Adapter<SalesReportRvAdapter.WordViewHolder> {

    private static final String TAG = "ProductAdapter";
    private final LayoutInflater mInflater;


    private List<Sales> mSales; // Cached copy of words
    private static ClickListener clickListener;

    SalesReportRvAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public WordViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_sales_report, parent, false);
        return new WordViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull WordViewHolder holder, int position) {
        if (mSales != null) {
            Sales current = mSales.get(position);
            Log.d(TAG, "onBindViewHolder: " + current.getId());
            holder.textProductName.setText(current.getProductSold());
            holder.textSalesDate.setText(Helper.getStringFromDate(current.getDateTime()));
            holder.textTotalCash.setText(Helper.formatRupiah(current.getTotalCash()));
        } else {
            // Covers the case of data not being ready yet.
            holder.textProductName.setText("Belum ada data");
        }
    }

    /**
     * Associates a list of words with this adapter
     */
    void setSales(List<Sales> sales) {
        mSales = sales;
        notifyDataSetChanged();
    }

    /**
     * getItemCount() is called many times, and when it is first called,
     * mSales has not been updated (means initially, it's null, and we can't return null).
     */
    @Override
    public int getItemCount() {
        if (mSales != null)
            return mSales.size();
        else return 0;
    }

    /**
     * Gets the word at a given position.
     * This method is useful for identifying which word
     * was clicked or swiped in methods that handle user events.
     *
     * @param position The position of the word in the RecyclerView
     * @return The word at the given position
     */
    public Sales getSalesAtPosition(int position) {
        return mSales.get(position);
    }

    class WordViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_sales_date)
        TextView textSalesDate;
        @BindView(R.id.text_product_name)
        TextView textProductName;
        @BindView(R.id.text_total_cash)
        TextView textTotalCash;
        @BindView(R.id.card_task)
        LinearLayout cardTask;

        private WordViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            //fixme onClick
            itemView.setOnClickListener(view -> clickListener.onItemClick(view, getAdapterPosition()));
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        SalesReportRvAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(View v, int position);
    }

}
