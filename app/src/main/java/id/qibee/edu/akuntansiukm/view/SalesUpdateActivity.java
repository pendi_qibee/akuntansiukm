package id.qibee.edu.akuntansiukm.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import id.qibee.edu.akuntansiukm.R;
import id.qibee.edu.akuntansiukm.db.Cart;

public class SalesUpdateActivity extends AppCompatActivity {
    private static final String TAG = "salesUpdate";
    int quantity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_update);
        Cart cart = (Cart) getIntent().getSerializableExtra("object");

        if (cart!=null){
            quantity=cart.getQuantity();
            Log.d(TAG, "onCreate: quantity= "+quantity);
        }
    }
}
