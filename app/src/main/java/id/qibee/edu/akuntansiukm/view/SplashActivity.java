package id.qibee.edu.akuntansiukm.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import id.qibee.edu.akuntansiukm.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }
}
