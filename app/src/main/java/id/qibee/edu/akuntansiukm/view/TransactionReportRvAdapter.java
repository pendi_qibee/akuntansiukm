/*
 * Copyright (C) 2018 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package id.qibee.edu.akuntansiukm.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.qibee.edu.akuntansiukm.R;
import id.qibee.edu.akuntansiukm.db.Transaction;
import id.qibee.edu.akuntansiukm.util.Helper;

/**
 * Adapter for the RecyclerView that displays a list of words.
 */

public class TransactionReportRvAdapter extends RecyclerView.Adapter<TransactionReportRvAdapter.TransactionViewHolder> {

    private static final String TAG = "Transaction Adapter";
    private final LayoutInflater mInflater;

    private List<Transaction> transactionList; // Cached copy of words
    private static ClickListener clickListener;

    public TransactionReportRvAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public TransactionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_transaction_report, parent, false);
        return new TransactionViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TransactionViewHolder holder, int position) {
        if (transactionList != null) {
            Transaction current = transactionList.get(position);
            Log.d(TAG, "onBindViewHolder: " + current.getId());

            holder.textTransactionDate.setText(Helper.getStringFromDate(current.getDate()));
            holder.textTransactionType.setText(current.getType());
            holder.textCashIn.setText(Helper.formatRupiah(current.getCashIn()));
            holder.textCashOut.setText(Helper.formatRupiah(current.getCashOut()));

        } else {
            // Covers the case of data not being ready yet.

        }
    }

    /**
     * Associates a list of words with this adapter
     */
    public void setTransactions(List<Transaction> transactions) {
        transactionList = transactions;
        notifyDataSetChanged();
    }

    /**
     * getItemCount() is called many times, and when it is first called,
     * transactionList has not been updated (means initially, it's null, and we can't return null).
     */
    @Override
    public int getItemCount() {
        if (transactionList != null)
            return transactionList.size();
        else return 0;
    }


    public Transaction getTransactionAtPosition(int position) {
        return transactionList.get(position);
    }

    class TransactionViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_transaction_date)
        TextView textTransactionDate;
        @BindView(R.id.text_transaction_type)
        TextView textTransactionType;
        @BindView(R.id.text_cash_in)
        TextView textCashIn;
        @BindView(R.id.text_cash_out)
        TextView textCashOut;

        private TransactionViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
//            itemView.setOnClickListener(view -> clickListener.onItemClick(view, getAdapterPosition()));
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        TransactionReportRvAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(View v, int position);
    }

}
