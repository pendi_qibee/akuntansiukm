package id.qibee.edu.akuntansiukm.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

import id.qibee.edu.akuntansiukm.db.Cart;
import id.qibee.edu.akuntansiukm.db.CartRepository;
import id.qibee.edu.akuntansiukm.db.TotalCash;

public class CartViewModel extends AndroidViewModel {
    private CartRepository  mCartRepository;
    private LiveData<List<Cart>> mAllCarts;
    private LiveData<List<Cart>> mInsertedProduct;

    public CartViewModel(Application application) {
        super(application);
        mCartRepository = new CartRepository(application);
        mAllCarts = mCartRepository.getAllItem();

    }

//    public CartViewModel(@NonNull Application application, int transactionId, int productId) {
//        super(application);
//        mCartRepository = new CartRepository(application);
//        mInsertedProduct=mCartRepository.getProductInserted(transactionId, productId);
//    }

    public LiveData<List<Cart>> getAllCarts() {
        return mAllCarts;
    }
    public LiveData<List<Cart>> getInsertedProduct(int transactionId, int productId) {
        return mCartRepository.getProductInserted(transactionId, productId);
    }

    public LiveData<List<Cart>> getCurrentCart(int transactionId) {
        return mCartRepository.getCurrentCart(transactionId);
    }

    public TotalCash getTotalCart(int transactionId) {
        return mCartRepository.getTotal(transactionId);
    }

    public void insertCarts(Cart cart) {
        mCartRepository.insertCart(cart);
    }

    public void updateCart(Cart cart) {
        mCartRepository.updateCart(cart);
    }

    public void deleteCart(Cart cart) {
        mCartRepository.deleteCart(cart);
    }

    public void deleteCartWithId(int id){
        mCartRepository.deleteCartWithId(id);
    }

    public void deleteAllCart() {
        mCartRepository.deleteAllCart();
    }

}
