package id.qibee.edu.akuntansiukm.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

import id.qibee.edu.akuntansiukm.db.Expense;
import id.qibee.edu.akuntansiukm.db.ExpenseRepository;
import id.qibee.edu.akuntansiukm.db.Month;
import id.qibee.edu.akuntansiukm.db.TotalExpense;

public class ExpenseViewModel extends AndroidViewModel {
    private ExpenseRepository expenseRepository;
    private LiveData<List<Expense>> mAllExpenses;
    private LiveData<List<Expense>> mMaxTransId;

    public ExpenseViewModel(Application application) {
        super(application);
        expenseRepository = new ExpenseRepository(application);
        mAllExpenses = expenseRepository.getAllExpenses();
        mMaxTransId = expenseRepository.getMaxTransId();

    }

    public LiveData<List<Expense>> getAllExpenses() {
        return mAllExpenses;
    }

    public LiveData<List<Expense>> getExpenseInMonth(String month) {
        return expenseRepository.getExpensesInMonth(month);
    }

    public LiveData<List<Expense>> getmMaxTransId() {
        return mMaxTransId;
    }

    public void insertExpenses(Expense expense) {
        expenseRepository.insertExpense(expense);
    }

    public void updateExpenses(Expense expense) {
        expenseRepository.updateExpense(expense);
    }

    public void deleteExpenses(Expense expense) {
        expenseRepository.deleteExpense(expense);
    }

    public int getMaxTransId() {
        expenseRepository.getMaxTransId();
        return 0;
    }

    public void deleteAllExpenses() {
        expenseRepository.deleteAllExpenses();
    }

    public LiveData<List<Month>> getMonth() {
        return expenseRepository.getMonth();
    }

    public TotalExpense getTotal() {
        return expenseRepository.getTotal();
    }

    public TotalExpense getTotalMonth(String month) {
        return expenseRepository.getTotalInMonth(month);
    }
}
