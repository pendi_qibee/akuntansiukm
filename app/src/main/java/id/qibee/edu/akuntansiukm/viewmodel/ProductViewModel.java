package id.qibee.edu.akuntansiukm.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

import id.qibee.edu.akuntansiukm.db.Product;
import id.qibee.edu.akuntansiukm.db.ProductRepository;

public class ProductViewModel extends AndroidViewModel {
    private ProductRepository mProductRepository;
    private LiveData<List<Product>> mAllProducts;

    public ProductViewModel(Application application) {
        super(application);
        mProductRepository = new ProductRepository(application);
        mAllProducts = mProductRepository.getAllProduct();

    }

    public LiveData<List<Product>> getAllProducts() {
        return mAllProducts;
    }

    public void insertProduct(Product product) {
        mProductRepository.insertProduct(product);
    }

    public void updateProduct(Product product) {
        mProductRepository.updateProduct(product);
    }

    public void updateStockbyId(int productId, int[] stockUpdate) {
        mProductRepository.updateStock(productId, stockUpdate);
    }

    public void deleteProduct(Product product) {
        mProductRepository.deleteProduct(product);
    }

    public void deleteAllProduct() {
        mProductRepository.deleteAllProduct();
    }

    public int getStock(int id){
       return mProductRepository.getStock(id);

    }

    public LiveData<Product> getProductWithId(int id){
        return mProductRepository.getDataWithId(id);
    }
}
