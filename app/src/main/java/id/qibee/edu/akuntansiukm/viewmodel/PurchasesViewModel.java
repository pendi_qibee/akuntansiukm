package id.qibee.edu.akuntansiukm.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

import id.qibee.edu.akuntansiukm.db.Month;
import id.qibee.edu.akuntansiukm.db.Purchase;
import id.qibee.edu.akuntansiukm.db.PurchaseRepository;
import id.qibee.edu.akuntansiukm.db.TotalPurchase;

public class PurchasesViewModel extends AndroidViewModel {
    private PurchaseRepository mPurchaseRepository;
    private LiveData<List<Purchase>> mAllPurchases;
    private LiveData<List<Purchase>> mMaxTransId;

    public PurchasesViewModel(Application application) {
        super(application);
        mPurchaseRepository = new PurchaseRepository(application);
        mAllPurchases = mPurchaseRepository.getAllPurchases();
        mMaxTransId = mPurchaseRepository.getMaxTransId();

    }

    public LiveData<List<Purchase>> getAllPurchases() {
        return mAllPurchases;
    }

    public LiveData<List<Purchase>> getmMaxTransId() {
        return mMaxTransId;
    }

    public void insertPurchases(Purchase purchase) {
        mPurchaseRepository.insertPurchase(purchase);
    }

    public void updatePurchases(Purchase purchase) {
        mPurchaseRepository.updatePurchase(purchase);
    }

    public void deletePurchases(Purchase purchase) {
        mPurchaseRepository.deletePurchase(purchase);
    }

    public int getMaxTransId() {
        mPurchaseRepository.getMaxTransId();
        return 0;
    }

    public void deleteAllPurchases() {
        mPurchaseRepository.deleteAllPurchases();
    }

    public LiveData<List<Month>> getMonth() {
        return mPurchaseRepository.getMonth();
    }

    public LiveData<List<Purchase>> getPurchasesInMonth(String month) {

        return mPurchaseRepository.getPurchasesInMonth(month);

    }

    public TotalPurchase getTotal() {
        return mPurchaseRepository.getTotal();
    }

    public TotalPurchase getTotalMonth(String month) {
        return mPurchaseRepository.getTotalInMonth(month);
    }

}
