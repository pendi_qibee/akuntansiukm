package id.qibee.edu.akuntansiukm.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

import id.qibee.edu.akuntansiukm.db.Month;
import id.qibee.edu.akuntansiukm.db.Sales;
import id.qibee.edu.akuntansiukm.db.SalesRepository;
import id.qibee.edu.akuntansiukm.db.TotalSales;

public class SalesViewModel extends AndroidViewModel {
    private SalesRepository  mSalesRepository;
    private LiveData<List<Sales>> mAllSales;
    private LiveData<List<Sales>> mMaxTransId;

    public SalesViewModel(Application application) {
        super(application);
        mSalesRepository = new SalesRepository(application);
        mAllSales = mSalesRepository.getAllSales();
        mMaxTransId=mSalesRepository.getMaxTransId();

    }

    public LiveData<List<Sales>> getAllSaleses() {
        return mAllSales;
    }

    public LiveData<List<Sales>> getSales(String month) {
        return mSalesRepository.getSalesInMonth(month);
    }

    public LiveData<List<Sales>> getmMaxTransId() {
        return mMaxTransId;
    }

    public void insertSales(Sales Sales) {
        mSalesRepository.insertSales(Sales);
    }

    public void updateSales(Sales Sales) {
        mSalesRepository.updateSales(Sales);
    }

    public void deleteSales(Sales Sales) {
        mSalesRepository.deleteSales(Sales);
    }
    public int getMaxTransId() {
        mSalesRepository.getMaxTransId();
        return 0;
    }

    public void deleteAllSales() {
        mSalesRepository.deleteAllSales();
    }
    public LiveData<List<Month>> getMonth() {
        return mSalesRepository.getMonth();
    }

    public TotalSales getTotal() {
        return mSalesRepository.getTotal();
    }


public TotalSales getTotalMonth(String month) {
        return mSalesRepository.getTotalInMonth(month);
    }
}
