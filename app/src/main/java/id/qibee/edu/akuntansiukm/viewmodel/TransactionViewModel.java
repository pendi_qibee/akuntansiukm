package id.qibee.edu.akuntansiukm.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

import id.qibee.edu.akuntansiukm.db.Month;
import id.qibee.edu.akuntansiukm.db.TotalTransaction;
import id.qibee.edu.akuntansiukm.db.Transaction;
import id.qibee.edu.akuntansiukm.db.TransactionRepository;

public class TransactionViewModel extends AndroidViewModel {
    private TransactionRepository transactionRepository;
    private LiveData<List<Transaction>> mAllTransactions;
    private LiveData<List<Transaction>> mMaxTransId;

    public TransactionViewModel(Application application) {
        super(application);
        transactionRepository = new TransactionRepository(application);
        mAllTransactions = transactionRepository.getAllTransactions();

    }

    public LiveData<List<Transaction>> getAllTransactions() {
        return mAllTransactions;
    }

    public LiveData<List<Transaction>> getSales(String month) {
        return transactionRepository.getTransactionsInMonth(month);
    }

    public LiveData<List<Transaction>> getmMaxTransId() {
        return mMaxTransId;
    }

    public void insertTransaction(Transaction Transaction) {
        transactionRepository.insertTransaction(Transaction);
    }

    public void updateTransaction(Transaction Transaction) {
        transactionRepository.updateTransaction(Transaction);
    }

    public void deleteTransaction(Transaction Transaction) {
        transactionRepository.deleteTransaction(Transaction);
    }

    public void deleteByCode(String transactionCode) {
        transactionRepository.deleteByTransactionCode(transactionCode);
    }

    public void deleteAllSales() {
        transactionRepository.deleteAllTransactions();
    }
    public LiveData<List<Month>> getMonth() {
        return transactionRepository.getMonth();
    }

    public TotalTransaction getTotalCashIn() {
        return transactionRepository.getTotalCashIn();
    }

    public TotalTransaction getTotalCashOut() {
        return transactionRepository.getTotalCashOut();
    }


//public TotalTransaction getTotalMonth(String month) {
//        return transactionRepository.getTotalInMonth(month);
//    }
}
